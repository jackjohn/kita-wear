import type { Schema, Attribute } from '@strapi/strapi';

export interface SharedProductSizeToQuantity extends Schema.Component {
	collectionName: 'c_s_product_size_to_quantities';
	info: {
		displayName: 'ProductSizeToQuantity';
		description: '';
	};
	attributes: {
		product_size: Attribute.Relation<
			'shared.product-size-to-quantity',
			'oneToOne',
			'api::product-size.product-size'
		>;
		Quantity: Attribute.String & Attribute.Required;
	};
}

export interface SharedSeo extends Schema.Component {
	collectionName: 'components_shared_seos';
	info: {
		displayName: 'Seo';
		icon: 'book';
	};
	attributes: {
		Title: Attribute.String;
		Description: Attribute.String;
		Keywords: Attribute.String;
		Image: Attribute.Media;
		PreventIndexing: Attribute.Boolean & Attribute.DefaultTo<false>;
	};
}

declare module '@strapi/types' {
	export module Shared {
		export interface Components {
			'shared.product-size-to-quantity': SharedProductSizeToQuantity;
			'shared.seo': SharedSeo;
		}
	}
}
