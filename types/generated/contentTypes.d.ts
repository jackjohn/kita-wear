import type { Schema, Attribute } from '@strapi/strapi';

export interface AdminPermission extends Schema.CollectionType {
	collectionName: 'admin_permissions';
	info: {
		name: 'Permission';
		description: '';
		singularName: 'permission';
		pluralName: 'permissions';
		displayName: 'Permission';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		action: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		actionParameters: Attribute.JSON & Attribute.DefaultTo<{}>;
		subject: Attribute.String &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		properties: Attribute.JSON & Attribute.DefaultTo<{}>;
		conditions: Attribute.JSON & Attribute.DefaultTo<[]>;
		role: Attribute.Relation<'admin::permission', 'manyToOne', 'admin::role'>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'admin::permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface AdminUser extends Schema.CollectionType {
	collectionName: 'admin_users';
	info: {
		name: 'User';
		description: '';
		singularName: 'user';
		pluralName: 'users';
		displayName: 'User';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		firstname: Attribute.String &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		lastname: Attribute.String &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		username: Attribute.String;
		email: Attribute.Email &
			Attribute.Required &
			Attribute.Private &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 6;
			}>;
		password: Attribute.Password &
			Attribute.Private &
			Attribute.SetMinMaxLength<{
				minLength: 6;
			}>;
		resetPasswordToken: Attribute.String & Attribute.Private;
		registrationToken: Attribute.String & Attribute.Private;
		isActive: Attribute.Boolean & Attribute.Private & Attribute.DefaultTo<false>;
		roles: Attribute.Relation<'admin::user', 'manyToMany', 'admin::role'> & Attribute.Private;
		blocked: Attribute.Boolean & Attribute.Private & Attribute.DefaultTo<false>;
		preferedLanguage: Attribute.String;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::user', 'oneToOne', 'admin::user'> & Attribute.Private;
		updatedBy: Attribute.Relation<'admin::user', 'oneToOne', 'admin::user'> & Attribute.Private;
	};
}

export interface AdminRole extends Schema.CollectionType {
	collectionName: 'admin_roles';
	info: {
		name: 'Role';
		description: '';
		singularName: 'role';
		pluralName: 'roles';
		displayName: 'Role';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.Required &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		code: Attribute.String &
			Attribute.Required &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		description: Attribute.String;
		users: Attribute.Relation<'admin::role', 'manyToMany', 'admin::user'>;
		permissions: Attribute.Relation<'admin::role', 'oneToMany', 'admin::permission'>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::role', 'oneToOne', 'admin::user'> & Attribute.Private;
		updatedBy: Attribute.Relation<'admin::role', 'oneToOne', 'admin::user'> & Attribute.Private;
	};
}

export interface AdminApiToken extends Schema.CollectionType {
	collectionName: 'strapi_api_tokens';
	info: {
		name: 'Api Token';
		singularName: 'api-token';
		pluralName: 'api-tokens';
		displayName: 'Api Token';
		description: '';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.Required &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		description: Attribute.String &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}> &
			Attribute.DefaultTo<''>;
		type: Attribute.Enumeration<['read-only', 'full-access', 'custom']> &
			Attribute.Required &
			Attribute.DefaultTo<'read-only'>;
		accessKey: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		lastUsedAt: Attribute.DateTime;
		permissions: Attribute.Relation<'admin::api-token', 'oneToMany', 'admin::api-token-permission'>;
		expiresAt: Attribute.DateTime;
		lifespan: Attribute.BigInteger;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::api-token', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'admin::api-token', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface AdminApiTokenPermission extends Schema.CollectionType {
	collectionName: 'strapi_api_token_permissions';
	info: {
		name: 'API Token Permission';
		description: '';
		singularName: 'api-token-permission';
		pluralName: 'api-token-permissions';
		displayName: 'API Token Permission';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		action: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		token: Attribute.Relation<'admin::api-token-permission', 'manyToOne', 'admin::api-token'>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::api-token-permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'admin::api-token-permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface AdminTransferToken extends Schema.CollectionType {
	collectionName: 'strapi_transfer_tokens';
	info: {
		name: 'Transfer Token';
		singularName: 'transfer-token';
		pluralName: 'transfer-tokens';
		displayName: 'Transfer Token';
		description: '';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.Required &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		description: Attribute.String &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}> &
			Attribute.DefaultTo<''>;
		accessKey: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		lastUsedAt: Attribute.DateTime;
		permissions: Attribute.Relation<
			'admin::transfer-token',
			'oneToMany',
			'admin::transfer-token-permission'
		>;
		expiresAt: Attribute.DateTime;
		lifespan: Attribute.BigInteger;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::transfer-token', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'admin::transfer-token', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface AdminTransferTokenPermission extends Schema.CollectionType {
	collectionName: 'strapi_transfer_token_permissions';
	info: {
		name: 'Transfer Token Permission';
		description: '';
		singularName: 'transfer-token-permission';
		pluralName: 'transfer-token-permissions';
		displayName: 'Transfer Token Permission';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		action: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 1;
			}>;
		token: Attribute.Relation<
			'admin::transfer-token-permission',
			'manyToOne',
			'admin::transfer-token'
		>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'admin::transfer-token-permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'admin::transfer-token-permission', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface PluginUploadFile extends Schema.CollectionType {
	collectionName: 'files';
	info: {
		singularName: 'file';
		pluralName: 'files';
		displayName: 'File';
		description: '';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String & Attribute.Required;
		alternativeText: Attribute.String;
		caption: Attribute.String;
		width: Attribute.Integer;
		height: Attribute.Integer;
		formats: Attribute.JSON;
		hash: Attribute.String & Attribute.Required;
		ext: Attribute.String;
		mime: Attribute.String & Attribute.Required;
		size: Attribute.Decimal & Attribute.Required;
		url: Attribute.String & Attribute.Required;
		previewUrl: Attribute.String;
		provider: Attribute.String & Attribute.Required;
		provider_metadata: Attribute.JSON;
		related: Attribute.Relation<'plugin::upload.file', 'morphToMany'>;
		folder: Attribute.Relation<'plugin::upload.file', 'manyToOne', 'plugin::upload.folder'> &
			Attribute.Private;
		folderPath: Attribute.String &
			Attribute.Required &
			Attribute.Private &
			Attribute.SetMinMax<{
				min: 1;
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'plugin::upload.file', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'plugin::upload.file', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface PluginUploadFolder extends Schema.CollectionType {
	collectionName: 'upload_folders';
	info: {
		singularName: 'folder';
		pluralName: 'folders';
		displayName: 'Folder';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMax<{
				min: 1;
			}>;
		pathId: Attribute.Integer & Attribute.Required & Attribute.Unique;
		parent: Attribute.Relation<'plugin::upload.folder', 'manyToOne', 'plugin::upload.folder'>;
		children: Attribute.Relation<'plugin::upload.folder', 'oneToMany', 'plugin::upload.folder'>;
		files: Attribute.Relation<'plugin::upload.folder', 'oneToMany', 'plugin::upload.file'>;
		path: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMax<{
				min: 1;
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'plugin::upload.folder', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'plugin::upload.folder', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface PluginI18NLocale extends Schema.CollectionType {
	collectionName: 'i18n_locale';
	info: {
		singularName: 'locale';
		pluralName: 'locales';
		collectionName: 'locales';
		displayName: 'Locale';
		description: '';
	};
	options: {
		draftAndPublish: false;
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.SetMinMax<{
				min: 1;
				max: 50;
			}>;
		code: Attribute.String & Attribute.Unique;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'plugin::i18n.locale', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'plugin::i18n.locale', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface PluginUsersPermissionsPermission extends Schema.CollectionType {
	collectionName: 'up_permissions';
	info: {
		name: 'permission';
		description: '';
		singularName: 'permission';
		pluralName: 'permissions';
		displayName: 'Permission';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		action: Attribute.String & Attribute.Required;
		role: Attribute.Relation<
			'plugin::users-permissions.permission',
			'manyToOne',
			'plugin::users-permissions.role'
		>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'plugin::users-permissions.permission',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'plugin::users-permissions.permission',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
	};
}

export interface PluginUsersPermissionsRole extends Schema.CollectionType {
	collectionName: 'up_roles';
	info: {
		name: 'role';
		description: '';
		singularName: 'role';
		pluralName: 'roles';
		displayName: 'Role';
	};
	pluginOptions: {
		'content-manager': {
			visible: false;
		};
		'content-type-builder': {
			visible: false;
		};
	};
	attributes: {
		name: Attribute.String &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 3;
			}>;
		description: Attribute.String;
		type: Attribute.String & Attribute.Unique;
		permissions: Attribute.Relation<
			'plugin::users-permissions.role',
			'oneToMany',
			'plugin::users-permissions.permission'
		>;
		users: Attribute.Relation<
			'plugin::users-permissions.role',
			'oneToMany',
			'plugin::users-permissions.user'
		>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'plugin::users-permissions.role', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'plugin::users-permissions.role', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface PluginUsersPermissionsUser extends Schema.CollectionType {
	collectionName: 'up_users';
	info: {
		name: 'user';
		description: '';
		singularName: 'user';
		pluralName: 'users';
		displayName: 'User';
	};
	options: {
		draftAndPublish: false;
		timestamps: true;
	};
	attributes: {
		username: Attribute.String &
			Attribute.Required &
			Attribute.Unique &
			Attribute.SetMinMaxLength<{
				minLength: 3;
			}>;
		email: Attribute.Email &
			Attribute.Required &
			Attribute.SetMinMaxLength<{
				minLength: 6;
			}>;
		provider: Attribute.String;
		password: Attribute.Password &
			Attribute.Private &
			Attribute.SetMinMaxLength<{
				minLength: 6;
			}>;
		resetPasswordToken: Attribute.String & Attribute.Private;
		confirmationToken: Attribute.String & Attribute.Private;
		confirmed: Attribute.Boolean & Attribute.DefaultTo<false>;
		blocked: Attribute.Boolean & Attribute.DefaultTo<false>;
		role: Attribute.Relation<
			'plugin::users-permissions.user',
			'manyToOne',
			'plugin::users-permissions.role'
		>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'plugin::users-permissions.user', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'plugin::users-permissions.user', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface ApiAboutAbout extends Schema.SingleType {
	collectionName: 'abouts';
	info: {
		singularName: 'about';
		pluralName: 'abouts';
		displayName: 'About';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		StartingDate: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		StartingDescription: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		QualityTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		QualityDescription: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Video: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::about.about', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::about.about', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<'api::about.about', 'oneToMany', 'api::about.about'>;
		locale: Attribute.String;
	};
}

export interface ApiClientOrderConfirmedEmailClientOrderConfirmedEmail extends Schema.SingleType {
	collectionName: 'client_order_confirmed_emails';
	info: {
		singularName: 'client-order-confirmed-email';
		pluralName: 'client-order-confirmed-emails';
		displayName: 'ClientOrderConfirmedEmail';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Subtitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		OrderConfirmation: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Total: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Address: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Footer: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Subject: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CompanyEmails: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingPrice: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Discount: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::client-order-confirmed-email.client-order-confirmed-email',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::client-order-confirmed-email.client-order-confirmed-email',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::client-order-confirmed-email.client-order-confirmed-email',
			'oneToMany',
			'api::client-order-confirmed-email.client-order-confirmed-email'
		>;
		locale: Attribute.String;
	};
}

export interface ApiClothingCarePageClothingCarePage extends Schema.SingleType {
	collectionName: 'clothing_care_pages';
	info: {
		singularName: 'clothing-care-page';
		pluralName: 'clothing-care-pages';
		displayName: 'ClothingCarePage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::clothing-care-page.clothing-care-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::clothing-care-page.clothing-care-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::clothing-care-page.clothing-care-page',
			'oneToMany',
			'api::clothing-care-page.clothing-care-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiContactUsPageContactUsPage extends Schema.SingleType {
	collectionName: 'contact_us_pages';
	info: {
		singularName: 'contact-us-page';
		pluralName: 'contact-us-pages';
		displayName: 'ContactUsPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::contact-us-page.contact-us-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::contact-us-page.contact-us-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::contact-us-page.contact-us-page',
			'oneToMany',
			'api::contact-us-page.contact-us-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiFaqFaq extends Schema.CollectionType {
	collectionName: 'faqs';
	info: {
		singularName: 'faq';
		pluralName: 'faqs';
		displayName: 'FAQ';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Description: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::faq.faq', 'oneToOne', 'admin::user'> & Attribute.Private;
		updatedBy: Attribute.Relation<'api::faq.faq', 'oneToOne', 'admin::user'> & Attribute.Private;
		localizations: Attribute.Relation<'api::faq.faq', 'oneToMany', 'api::faq.faq'>;
		locale: Attribute.String;
	};
}

export interface ApiFooterFooter extends Schema.SingleType {
	collectionName: 'footers';
	info: {
		singularName: 'footer';
		pluralName: 'footers';
		displayName: 'Footer';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		PublicOfferAgreement: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PrivacyPolicy: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingAndPayment: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ClothingCare: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ReturnAndExchange: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ContactUs: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InstagramTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InstagramLink: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FacebookTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FacebookLink: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		About: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::footer.footer', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::footer.footer', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<'api::footer.footer', 'oneToMany', 'api::footer.footer'>;
		locale: Attribute.String;
	};
}

export interface ApiHomepageHomepage extends Schema.SingleType {
	collectionName: 'homepages';
	info: {
		singularName: 'homepage';
		pluralName: 'homepages';
		displayName: 'Homepage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ForHim: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ForHer: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		LeftPhoto: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		RightPhoto: Attribute.Media &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::homepage.homepage', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::homepage.homepage', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::homepage.homepage',
			'oneToMany',
			'api::homepage.homepage'
		>;
		locale: Attribute.String;
	};
}

export interface ApiNavigationNavigation extends Schema.SingleType {
	collectionName: 'navigations';
	info: {
		singularName: 'navigation';
		pluralName: 'navigations';
		displayName: 'Navigation';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		ProductsMenTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		AboutTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CenterText: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InstagramTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InstagramLink: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FacebookTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FacebookLink: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ProductsWomenTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::navigation.navigation', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::navigation.navigation', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::navigation.navigation',
			'oneToMany',
			'api::navigation.navigation'
		>;
		locale: Attribute.String;
	};
}

export interface ApiNotFoundPageNotFoundPage extends Schema.SingleType {
	collectionName: 'not_found_pages';
	info: {
		singularName: 'not-found-page';
		pluralName: 'not-found-pages';
		displayName: 'NotFoundPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		SomethingWrongLabel: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		BackToHomePageLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::not-found-page.not-found-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::not-found-page.not-found-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::not-found-page.not-found-page',
			'oneToMany',
			'api::not-found-page.not-found-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiOrderOrder extends Schema.CollectionType {
	collectionName: 'orders';
	info: {
		singularName: 'order';
		pluralName: 'orders';
		displayName: 'Order';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	attributes: {
		FirstName: Attribute.String & Attribute.Required;
		LastName: Attribute.String & Attribute.Required;
		Email: Attribute.Email & Attribute.Required;
		PhoneNumber: Attribute.String & Attribute.Required;
		InpostOfficePoint: Attribute.JSON & Attribute.Required;
		Status: Attribute.Enumeration<
			['CREATED', 'NEW', 'PENDING', 'COMPLETED', 'CANCELED', 'WAITING_FOR_CONFIRMATION']
		> &
			Attribute.Required;
		Products: Attribute.JSON & Attribute.Required;
		ShippingPrice: Attribute.Decimal;
		TotalPrice: Attribute.Decimal & Attribute.Required;
		CustomerIP: Attribute.String;
		CurrencyCode: Attribute.String & Attribute.Required;
		Buyer: Attribute.JSON;
		PayMethod: Attribute.JSON;
		Properties: Attribute.JSON;
		PayuId: Attribute.UID;
		OrderId: Attribute.UID & Attribute.Required;
		promo_code: Attribute.Relation<'api::order.order', 'oneToOne', 'api::promo-code.promo-code'>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::order.order', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::order.order', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface ApiOrderCheckoutOrderCheckout extends Schema.SingleType {
	collectionName: 'order_checkouts';
	info: {
		singularName: 'order-checkout';
		pluralName: 'order-checkouts';
		displayName: 'OrderCheckoutPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ProductDetailsLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PriceLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		QuantityLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ColorLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SubtotalLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FreeShippingLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FreeShippingFrom: Attribute.Integer &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		DeliveryTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		NameLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SurnameLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PhoneNumberLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		EmailLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InpostOfficeLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		TotalLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PromoCodePlaceholder: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PromoCodeLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PayLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SizeLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		FreeShippingDescription: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		EmptyCartLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingPrice: Attribute.Decimal &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PromoCodeValidNotification: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PromoCodeInvalidNotification: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		InpostLoadError: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PrivacyAgreementLabel: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SubmitError: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::order-checkout.order-checkout', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::order-checkout.order-checkout', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::order-checkout.order-checkout',
			'oneToMany',
			'api::order-checkout.order-checkout'
		>;
		locale: Attribute.String;
	};
}

export interface ApiOrderStatusPageOrderStatusPage extends Schema.SingleType {
	collectionName: 'order_status_pages';
	info: {
		singularName: 'order-status-page';
		pluralName: 'order-status-pages';
		displayName: 'OrderStatusPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		PendingTitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PendingSubtitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		WaitingForConfirmationTitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		WaitingForConfirmationSubtitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CompletedTitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CompletedSubtitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CancelledTitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CancelledSubtitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		NotFoundTitle: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		NotFoundSubtitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::order-status-page.order-status-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::order-status-page.order-status-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::order-status-page.order-status-page',
			'oneToMany',
			'api::order-status-page.order-status-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiOurInstaOurInsta extends Schema.SingleType {
	collectionName: 'our_instas';
	info: {
		singularName: 'our-insta';
		pluralName: 'our-instas';
		displayName: 'Our insta';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		LinkTitle: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Link: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		TopLeftImage: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		BottomLeftImage: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		TopRightImage: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		BottomRightImage: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		CenterImage: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::our-insta.our-insta', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::our-insta.our-insta', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::our-insta.our-insta',
			'oneToMany',
			'api::our-insta.our-insta'
		>;
		locale: Attribute.String;
	};
}

export interface ApiPrivacyPolicyPagePrivacyPolicyPage extends Schema.SingleType {
	collectionName: 'privacy_policy_pages';
	info: {
		singularName: 'privacy-policy-page';
		pluralName: 'privacy-policy-pages';
		displayName: 'PrivacyPolicyPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::privacy-policy-page.privacy-policy-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::privacy-policy-page.privacy-policy-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::privacy-policy-page.privacy-policy-page',
			'oneToMany',
			'api::privacy-policy-page.privacy-policy-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiProductProduct extends Schema.CollectionType {
	collectionName: 'products';
	info: {
		singularName: 'product';
		pluralName: 'products';
		displayName: 'Product';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Description: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ModelParameters: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CompositionAndCare: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		MainPhoto: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		BackgroundPhoto: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		Photos: Attribute.Media &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		product_type: Attribute.Relation<
			'api::product.product',
			'manyToOne',
			'api::product-type.product-type'
		>;
		product_gender: Attribute.Relation<
			'api::product.product',
			'oneToOne',
			'api::product-gender.product-gender'
		>;
		Slug: Attribute.UID<'api::product.product', 'Title'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Color: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}>;
		RelatedProducts: Attribute.Relation<
			'api::product.product',
			'oneToMany',
			'api::product.product'
		>;
		FreeShipping: Attribute.Boolean &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: false;
				};
			}> &
			Attribute.DefaultTo<false>;
		Price: Attribute.Decimal &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ProductSizeToQuantity: Attribute.Component<'shared.product-size-to-quantity', true> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::product.product', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::product.product', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<'api::product.product', 'oneToMany', 'api::product.product'>;
		locale: Attribute.String;
	};
}

export interface ApiProductGenderProductGender extends Schema.CollectionType {
	collectionName: 'product_genders';
	info: {
		singularName: 'product-gender';
		pluralName: 'product-genders';
		displayName: 'ProductGender';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Name: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Slug: Attribute.UID<'api::product-gender.product-gender', 'Name'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::product-gender.product-gender', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::product-gender.product-gender', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::product-gender.product-gender',
			'oneToMany',
			'api::product-gender.product-gender'
		>;
		locale: Attribute.String;
	};
}

export interface ApiProductPageProductPage extends Schema.SingleType {
	collectionName: 'product_pages';
	info: {
		singularName: 'product-page';
		pluralName: 'product-pages';
		displayName: 'ProductPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		ColorLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		PriceLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ProductDescriptionLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ModelParametersLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		CompositionAndCareLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingAndPaymentLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ShippingAndPaymentContent: Attribute.Text &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		RelatedProductsLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		RelatedProductsSublabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		AddToCartLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		OneClickBuyLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		ProductAddedToCartMessage: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SelectSizeLabel: Attribute.String &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::product-page.product-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::product-page.product-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::product-page.product-page',
			'oneToMany',
			'api::product-page.product-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiProductSizeProductSize extends Schema.CollectionType {
	collectionName: 'product_sizes';
	info: {
		singularName: 'product-size';
		pluralName: 'product-sizes';
		displayName: 'ProductSize';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	attributes: {
		Title: Attribute.String & Attribute.Required;
		Slug: Attribute.UID<'api::product-size.product-size', 'Title'>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::product-size.product-size', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::product-size.product-size', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface ApiProductTypeProductType extends Schema.CollectionType {
	collectionName: 'product_types';
	info: {
		singularName: 'product-type';
		pluralName: 'product-types';
		displayName: 'ProductType';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Name: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Slug: Attribute.UID<'api::product-type.product-type', 'Name'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		products: Attribute.Relation<
			'api::product-type.product-type',
			'oneToMany',
			'api::product.product'
		>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::product-type.product-type', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::product-type.product-type', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::product-type.product-type',
			'oneToMany',
			'api::product-type.product-type'
		>;
		locale: Attribute.String;
	};
}

export interface ApiProductsPageProductsPage extends Schema.SingleType {
	collectionName: 'products_pages';
	info: {
		singularName: 'products-page';
		pluralName: 'products-pages';
		displayName: 'ProductsPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		MenCollection: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		WomenCollection: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::products-page.products-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::products-page.products-page', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::products-page.products-page',
			'oneToMany',
			'api::products-page.products-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiProductsSortAndFilterProductsSortAndFilter extends Schema.SingleType {
	collectionName: 'products_sort_and_filters';
	info: {
		singularName: 'products-sort-and-filter';
		pluralName: 'products-sort-and-filters';
		displayName: 'ProductsSortAndFilter';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		SortLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SortLastUpdatedLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SortHighPriceLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		SortLowPriceLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		OnlyInStockLabel: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::products-sort-and-filter.products-sort-and-filter',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::products-sort-and-filter.products-sort-and-filter',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::products-sort-and-filter.products-sort-and-filter',
			'oneToMany',
			'api::products-sort-and-filter.products-sort-and-filter'
		>;
		locale: Attribute.String;
	};
}

export interface ApiPromoCodePromoCode extends Schema.CollectionType {
	collectionName: 'promo_codes';
	info: {
		singularName: 'promo-code';
		pluralName: 'promo-codes';
		displayName: 'PromoCode';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	attributes: {
		Code: Attribute.String & Attribute.Required;
		DiscountPercentage: Attribute.Decimal & Attribute.Required;
		Count: Attribute.String & Attribute.Required;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<'api::promo-code.promo-code', 'oneToOne', 'admin::user'> &
			Attribute.Private;
		updatedBy: Attribute.Relation<'api::promo-code.promo-code', 'oneToOne', 'admin::user'> &
			Attribute.Private;
	};
}

export interface ApiPublicOfferAgreementPagePublicOfferAgreementPage extends Schema.SingleType {
	collectionName: 'public_offer_agreement_pages';
	info: {
		singularName: 'public-offer-agreement-page';
		pluralName: 'public-offer-agreement-pages';
		displayName: 'PublicOfferAgreementPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::public-offer-agreement-page.public-offer-agreement-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::public-offer-agreement-page.public-offer-agreement-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::public-offer-agreement-page.public-offer-agreement-page',
			'oneToMany',
			'api::public-offer-agreement-page.public-offer-agreement-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiReturnAndExchangePageReturnAndExchangePage extends Schema.SingleType {
	collectionName: 'return_and_exchange_pages';
	info: {
		singularName: 'return-and-exchange-page';
		pluralName: 'return-and-exchange-pages';
		displayName: 'ReturnAndExchangePage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::return-and-exchange-page.return-and-exchange-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::return-and-exchange-page.return-and-exchange-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::return-and-exchange-page.return-and-exchange-page',
			'oneToMany',
			'api::return-and-exchange-page.return-and-exchange-page'
		>;
		locale: Attribute.String;
	};
}

export interface ApiShippingAndPaymentPageShippingAndPaymentPage extends Schema.SingleType {
	collectionName: 'shipping_and_payment_pages';
	info: {
		singularName: 'shipping-and-payment-page';
		pluralName: 'shipping-and-payment-pages';
		displayName: 'ShippingAndPaymentPage';
		description: '';
	};
	options: {
		draftAndPublish: true;
	};
	pluginOptions: {
		i18n: {
			localized: true;
		};
	};
	attributes: {
		Title: Attribute.String &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Content: Attribute.RichText &
			Attribute.Required &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		Seo: Attribute.Component<'shared.seo'> &
			Attribute.SetPluginOptions<{
				i18n: {
					localized: true;
				};
			}>;
		createdAt: Attribute.DateTime;
		updatedAt: Attribute.DateTime;
		publishedAt: Attribute.DateTime;
		createdBy: Attribute.Relation<
			'api::shipping-and-payment-page.shipping-and-payment-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		updatedBy: Attribute.Relation<
			'api::shipping-and-payment-page.shipping-and-payment-page',
			'oneToOne',
			'admin::user'
		> &
			Attribute.Private;
		localizations: Attribute.Relation<
			'api::shipping-and-payment-page.shipping-and-payment-page',
			'oneToMany',
			'api::shipping-and-payment-page.shipping-and-payment-page'
		>;
		locale: Attribute.String;
	};
}

declare module '@strapi/types' {
	export module Shared {
		export interface ContentTypes {
			'admin::permission': AdminPermission;
			'admin::user': AdminUser;
			'admin::role': AdminRole;
			'admin::api-token': AdminApiToken;
			'admin::api-token-permission': AdminApiTokenPermission;
			'admin::transfer-token': AdminTransferToken;
			'admin::transfer-token-permission': AdminTransferTokenPermission;
			'plugin::upload.file': PluginUploadFile;
			'plugin::upload.folder': PluginUploadFolder;
			'plugin::i18n.locale': PluginI18NLocale;
			'plugin::users-permissions.permission': PluginUsersPermissionsPermission;
			'plugin::users-permissions.role': PluginUsersPermissionsRole;
			'plugin::users-permissions.user': PluginUsersPermissionsUser;
			'api::about.about': ApiAboutAbout;
			'api::client-order-confirmed-email.client-order-confirmed-email': ApiClientOrderConfirmedEmailClientOrderConfirmedEmail;
			'api::clothing-care-page.clothing-care-page': ApiClothingCarePageClothingCarePage;
			'api::contact-us-page.contact-us-page': ApiContactUsPageContactUsPage;
			'api::faq.faq': ApiFaqFaq;
			'api::footer.footer': ApiFooterFooter;
			'api::homepage.homepage': ApiHomepageHomepage;
			'api::navigation.navigation': ApiNavigationNavigation;
			'api::not-found-page.not-found-page': ApiNotFoundPageNotFoundPage;
			'api::order.order': ApiOrderOrder;
			'api::order-checkout.order-checkout': ApiOrderCheckoutOrderCheckout;
			'api::order-status-page.order-status-page': ApiOrderStatusPageOrderStatusPage;
			'api::our-insta.our-insta': ApiOurInstaOurInsta;
			'api::privacy-policy-page.privacy-policy-page': ApiPrivacyPolicyPagePrivacyPolicyPage;
			'api::product.product': ApiProductProduct;
			'api::product-gender.product-gender': ApiProductGenderProductGender;
			'api::product-page.product-page': ApiProductPageProductPage;
			'api::product-size.product-size': ApiProductSizeProductSize;
			'api::product-type.product-type': ApiProductTypeProductType;
			'api::products-page.products-page': ApiProductsPageProductsPage;
			'api::products-sort-and-filter.products-sort-and-filter': ApiProductsSortAndFilterProductsSortAndFilter;
			'api::promo-code.promo-code': ApiPromoCodePromoCode;
			'api::public-offer-agreement-page.public-offer-agreement-page': ApiPublicOfferAgreementPagePublicOfferAgreementPage;
			'api::return-and-exchange-page.return-and-exchange-page': ApiReturnAndExchangePageReturnAndExchangePage;
			'api::shipping-and-payment-page.shipping-and-payment-page': ApiShippingAndPaymentPageShippingAndPaymentPage;
		}
	}
}
