#!/bin/bash

APP_ROOT_DIR="$(dirname "$0")"
APP_ROOT_DIR=$(realpath "$APP_ROOT_DIR")
APP_ROOT_DIR="$(dirname "$APP_ROOT_DIR")"

docker compose -f $APP_ROOT_DIR/docker-compose.dev.yml run dev-app bash
