#!/bin/bash

APP_ROOT_DIR="$(dirname "$0")"
APP_ROOT_DIR=$(realpath "$APP_ROOT_DIR")
APP_ROOT_DIR="$(dirname "$APP_ROOT_DIR")"

cd $APP_ROOT_DIR

. $APP_ROOT_DIR/.env

echo "Pulling latest changes..."
git pull origin main

echo "Building new Docker images..."
sh $APP_ROOT_DIR/scripts/build-prod.sh

echo "Stopping old containers..."
docker compose -f $APP_ROOT_DIR/docker-compose.yml down

echo "Starting new containers..."
sh $APP_ROOT_DIR/scripts/run-prod.sh

echo "Cleaning up unused Docker images, containers, volumes..."
docker system prune -a -f
docker volume prune -f

volumes=$(docker volume ls -qf dangling=true)
for volume in $volumes
do
  echo "Removing volume: $volume"
  docker volume rm $volume
done

STRAPI_ADMIN_URL="${APP_BASE_URL}/strapi/admin"

count=0

while [ $count -lt 10 ]; do
  STATUS=$(curl -s -o /dev/null -w '%{http_code}' $STRAPI_ADMIN_URL)

  if [ $STATUS -eq 200 ]; then
    echo "Strapi app is ready"
    break
  else
    echo "Waiting for Strapi app to initialize: $STRAPI_ADMIN_URL"
    sleep 5
    count=$((count+1))
  fi
done

APP_CONTAINER_ID=$(docker ps -qf "name=app")

if [ -z "$APP_CONTAINER_ID" ]
then
  echo "No Docker container with the name 'app' was found."
  exit 1
else
  echo "Building strapi admin panel..."
  docker exec $APP_CONTAINER_ID bash -c "rm -rf build && rm -rf .cache && npm run strapi-build"
  docker compose -f $APP_ROOT_DIR/docker-compose.yml stop
  sh $APP_ROOT_DIR/scripts/run-prod.sh

  count=0

  while [ $count -lt 10 ]; do
    STATUS=$(curl -s -o /dev/null -w '%{http_code}' $STRAPI_ADMIN_URL)

    if [ $STATUS -eq 200 ]; then
      echo "Strapi app is ready"
      break
    else
      echo "Waiting for Strapi app to initialize: $STRAPI_ADMIN_URL"
      sleep 5
      count=$((count+1))
    fi
  done

  APP_CONTAINER_ID=$(docker ps -qf "name=app")

  echo "Building frontend..."
  docker exec $APP_CONTAINER_ID bash -c "npm run build:client"

  echo "Finished successfully"
fi