#!/bin/bash

APP_ROOT_DIR="$(dirname "$0")"
APP_ROOT_DIR=$(realpath "$APP_ROOT_DIR")
APP_ROOT_DIR="$(dirname "$APP_ROOT_DIR")"

docker compose -f $APP_ROOT_DIR/docker-compose.dev.yml up postgres -d

until docker-compose exec -T postgres pg_isready; do
  echo 'Waiting for postgres database to become available...'
  sleep 1
done

echo 'Postgres database is available...'

CONTAINER_ID=$(docker ps -qf "name=postgres")

echo 'Dropping strapi database...'
docker exec -i $CONTAINER_ID psql -U root -d postgres -c "DROP DATABASE IF EXISTS strapi;"

echo 'Creating strapi database...'
docker exec -i $CONTAINER_ID psql -U root -d postgres -c "CREATE DATABASE strapi;"

echo 'Filling strapi database...'
docker exec -i $CONTAINER_ID psql -U root -d strapi < $1

echo 'Stopping postgres...'
docker compose -f $APP_ROOT_DIR/docker-compose.dev.yml stop