import { vi } from 'vitest';
import { runtimeConfigMock } from './frontend/utils/tests/mocks';

vi.mock('#imports', async (importOriginal) => {
	return {
		...(await importOriginal()),
		useRuntimeConfig() {
			return runtimeConfigMock;
		},
	};
});
