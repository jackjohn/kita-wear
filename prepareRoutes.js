require('dotenv').config();
const axios = require('axios');
const fs = require('fs');

const apiCalls = [
	{
		path: 'product-types',
		handler: (data) => {
			const manPaths = data.data.data.map(({ attributes }) => `/products/man/${attributes.Slug}`);
			const womanPaths = data.data.data.map(
				({ attributes }) => `/products/woman/${attributes.Slug}`
			);

			return [...manPaths, ...womanPaths];
		},
	},
	{
		path: 'products',
		handler: (data) => {
			return data.data.data.map(({ attributes }) => `/product/${attributes.Slug}`);
		},
	},
];

Promise.all(
	apiCalls.map(({ path, handler }) => {
		return axios
			.get(`http://localhost:${process.env.PORT}/strapi/api/${path}`, {
				headers: {
					Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
				},
			})
			.then(handler)
			.catch((e) => {
				console.log('Error occured during API call', e);
			});
	})
)
	.then((items) => {
		const routes = items;
		fs.writeFileSync('routes.json', JSON.stringify(routes.flat()));
	})
	.catch((err) => {
		console.log('some error:', err);
	});
