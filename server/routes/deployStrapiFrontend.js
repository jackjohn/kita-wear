const shell = require('shelljs');
const axios = require('axios');
let buildProcess = null;

async function deployStrapiFrontend(req, res) {
	try {
		await axios({
			method: 'GET',
			url: `${process.env.APP_BASE_URL}/strapi/admin/users/me`,
			headers: {
				Authorization: req.headers.authorization,
			},
		});
	} catch (e) {
		return res.status(403).send(
			JSON.stringify({
				errorMessage: 'Failed authorization',
			}),
		);
	}

	if (buildProcess !== null) {
		return res.status(409).send(
			JSON.stringify({
				errorMessage: 'Deploy is currently in progress.\nPlease wait a few minutes and try again.',
			}),
		);
	}

	buildProcess = shell.exec(`npm run build:client`, { async: true });

	buildProcess.on('error', (error) => {
		console.error('Deploy finished with error:', error);
		buildProcess = null;
	});

	buildProcess.on('close', () => {
		console.log('Deploy finished successfully');
		buildProcess = null;
	});

	res.status(200).send(
		JSON.stringify({
			success: true,
		}),
	);
}

module.exports = { deployStrapiFrontend };
