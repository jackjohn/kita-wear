const createPayuOrder = require('../services/createPayuOrder');
const createStrapiOrder = require('../services/createStrapiOrder');
const getStrapiOrder = require('../services/getStrapiOrder');
const getStrapiProduct = require('../services/getStrapiProduct');
const getStrapiPromoCode = require('../services/getStrapiPromoCode');
const updateStrapiOrder = require('../services/updateStrapiOrder');
const updateStrapiProduct = require('../services/updateStrapiProduct');
const updateStrapiPromoCode = require('../services/updateStrapiPromoCode');
const validateOrderBody = require('../services/validateOrderBody');
const { ORDER_STATUS } = require('../utils/constants');

async function postOrder(req, res) {
	const payload = req.body;
	const { error: validateOrderBodyError } = await validateOrderBody(payload);

	if (validateOrderBodyError) {
		return res.status(400).send(validateOrderBodyError);
	}

	const remoteAddress = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).split(
		', ',
	);
	const { data: createdStrapiOrder, error: createdStrapiOrderError } = await createStrapiOrder(
		payload,
		remoteAddress.length > 1 ? remoteAddress[1] : remoteAddress[0],
	);

	if (createdStrapiOrderError) {
		return res.status(400).send({
			error: createdStrapiOrderError,
		});
	}

	const { data: strapiOrder, error: strapiOrderError } = await getStrapiOrder(
		createdStrapiOrder.id,
	);

	if (strapiOrderError) {
		return res.status(400).send({
			error: strapiOrderError,
		});
	}

	const { data: payuOrder, error: payuOrderError } = await createPayuOrder(strapiOrder);

	if (payuOrderError) {
		return res.status(400).send({
			error: payuOrderError,
		});
	}

	const { error: updatedStrapiOrderError } = await updateStrapiOrder(strapiOrder.id, {
		PayuId: payuOrder.id,
		Status: ORDER_STATUS.NEW,
	});

	if (updatedStrapiOrderError) {
		return res.status(400).send({
			error: updatedStrapiOrderError,
		});
	}

	for (const product of payload.products) {
		const { data: strapiProduct, error: strapiProductError } = await getStrapiProduct(product.id);

		if (strapiProductError) {
			return res.status(400).send({
				error: strapiProductError,
			});
		}

		const { error: updatedStrapiProductError } = await updateStrapiProduct(strapiProduct.id, {
			ProductSizeToQuantity: strapiProduct.attributes.ProductSizeToQuantity.map((item) => {
				if (item.product_size.data.id === product.sizeId) {
					return {
						...item,
						Quantity: `${parseInt(item.Quantity) - product.quantity}`,
					};
				}

				return item;
			}),
		});

		if (updatedStrapiProductError) {
			return res.status(400).send({
				error: updatedStrapiProductError,
			});
		}
	}

	if (strapiOrder.attributes.promo_code.data) {
		const { data: promoCode, error: promoCodeError } = await getStrapiPromoCode(
			strapiOrder.attributes.promo_code.data.attributes.Code,
		);

		if (promoCodeError) {
			return res.status(400).send({
				error: promoCodeError,
			});
		}

		await updateStrapiPromoCode(promoCode.id, {
			Count: `${parseInt(promoCode.attributes.Count) - 1}`,
		});
	}

	res.status(200).send({
		payuRedirectUrl: payuOrder.redirectUri,
	});
}

async function getOrderStatus(req, res) {
	const { data: order, error: orderError } = await getStrapiOrder(req.params.id);

	if (orderError) {
		return res.status(404).send(orderError);
	}

	res.status(200).send({
		status: order.attributes.Status,
	});
}

module.exports = {
	postOrder,
	getOrderStatus,
};
