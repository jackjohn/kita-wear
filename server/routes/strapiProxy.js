const proxy = require('express-http-proxy');
const isMultipartRequest = function (req) {
	let contentTypeHeader = req.headers['content-type'];
	return contentTypeHeader && contentTypeHeader.indexOf('multipart') > -1;
};

function strapiProxy(req, res, next) {
	let parseReqBody = true;
	if (isMultipartRequest(req)) {
		parseReqBody = false;
	}

	return proxy(`http://${process.env.NODE_ENV === 'production' ? '127.0.0.1' : 'localhost'}:1337`, {
		proxyReqPathResolver: (req) => {
			if (req.url.includes('api/promo-code')) {
				return null;
			}
			if (req.url.includes('api/order/')) {
				return null;
			}
			return req.url;
		},
		limit: '200mb',
		parseReqBody,
	})(req, res, next);
}

module.exports = { strapiProxy };
