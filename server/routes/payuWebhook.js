const updateStrapiOrder = require('../services/updateStrapiOrder');
const getStrapiOrder = require('../services/getStrapiOrder');
const { ORDER_STATUS } = require('../utils/constants');
const getStrapiProduct = require('../services/getStrapiProduct');
const updateStrapiProduct = require('../services/updateStrapiProduct');
const sendOrderConfirmedEmails = require('../services/sendOrderConfirmedEmails');
const getStrapiProducts = require('../services/getStrapiProducts');
const getStrapiPromoCode = require('../services/getStrapiPromoCode');
const updateStrapiPromoCode = require('../services/updateStrapiPromoCode');
const md5 = require('md5');

async function payuWebhook(req, res) {
	const signatureHeader = req.get('OpenPayu-Signature') || '';
	const signatureItem = signatureHeader
		.split(';')
		.find((item) => item.split('=')[0] === 'signature');
	const signature = signatureItem?.split('=')[1];
	const hash = md5(req.rawBody + process.env.PAYU_SECOND_KEY);

	if (!hash || !signature || hash !== signature) {
		return res.status(400).send('Invalid notification signature');
	}

	const data = req.body;

	const { buyer, properties, payMethod, status } = data.order;
	const updateData = { Status: status };

	if (status === ORDER_STATUS.CANCELED) {
		await reverseProductsCountChange(data.order.extOrderId);
	}

	if (status === ORDER_STATUS.COMPLETED) {
		const { data: strapiOrder, error: strapiOrderError } = await getStrapiOrder(
			data.order.extOrderId,
		);
		const { data: strapiProducts, error: strapiProductsError } = await getStrapiProducts();

		if (!strapiOrderError && !strapiProductsError) {
			try {
				await sendOrderConfirmedEmails(strapiOrder, strapiProducts);
			} catch (e) {
				console.error('Error sending emails:', e);
			}
		}
	}

	if (buyer) {
		updateData.Buyer = buyer;
	}
	if (properties) {
		updateData.Properties = properties;
	}
	if (payMethod) {
		updateData.PayMethod = payMethod;
	}

	const { error: updatedStrapiOrderError } = await updateStrapiOrder(
		data.order.extOrderId,
		updateData,
	);

	if (updatedStrapiOrderError) {
		return res.status(400).send();
	}

	res.status(200).send();
}

async function reverseProductsCountChange(orderId) {
	const { data: strapiOrder, error: strapiOrderError } = await getStrapiOrder(orderId);

	if (strapiOrderError) {
		return;
	}

	for (const product of strapiOrder.attributes.Products) {
		const { data: strapiProduct, error: strapiProductError } = await getStrapiProduct(product.id);

		if (strapiProductError) {
			continue;
		}

		await updateStrapiProduct(strapiProduct.id, {
			ProductSizeToQuantity: strapiProduct.attributes.ProductSizeToQuantity.map((item) => {
				if (item.product_size.data.id === product.sizeId) {
					return {
						...item,
						Quantity: `${parseInt(item.Quantity) + product.quantity}`,
					};
				}

				return item;
			}),
		});
	}

	if (strapiOrder.attributes.promo_code.data) {
		const { data: promoCode, error: promoCodeError } = await getStrapiPromoCode(
			strapiOrder.attributes.promo_code.data.attributes.Code,
		);

		if (promoCodeError) {
			return;
		}

		await updateStrapiPromoCode(promoCode.id, {
			Count: `${parseInt(promoCode.attributes.Count) + 1}`,
		});
	}
}

module.exports = { payuWebhook };
