const getStrapiPromoCode = require('../services/getStrapiPromoCode');

async function getPromoCode(req, res) {
	const { data: promoCode, error: promoCodeError } = await getStrapiPromoCode(req.params.code);

	if (promoCodeError) {
		return res.status(404).send(promoCodeError);
	}

	res.status(200).send({
		discountPercentage: promoCode.attributes.DiscountPercentage,
	});
}

module.exports = {
	getPromoCode,
};
