module.exports = function getClientOrderEmailHTML({
	tableItems,
	totalPrice,
	name,
	address,
	phoneNumber,
	email,
	logo,
	labels,
}) {
	return `<style type="text/css">
    table,
    td,
    a {
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
  
    table,
    td {
      mso-table-lspace: 0;
      mso-table-rspace: 0;
    }
  
    img {
      -ms-interpolation-mode: bicubic;
    }
  
    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
    }
  
    table {
      border-collapse: collapse !important;
    }
  
    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
      font-size: inherit !important;
      font-family: inherit !important;
      font-weight: inherit !important;
      line-height: inherit !important;
    }
  
    div[style*="margin: 16px 0;"] {
      margin: 0 !important;
    }
  </style>
  <div style="margin: 0 !important; padding: 20px 0 !important; background-color: #eeeeee" bgcolor="#eeeeee">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td align="center" style="background-color: #eeeeee" bgcolor="#eeeeee">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px">
            <tr>
              <td align="center" style="padding: 0 35px 20px 35px; background-color: #ffffff" bgcolor="#ffffff">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px">
                  <tr>
                    <td align="center" style="padding-top: 25px;">
                      <img src="${logo.src}" width="${logo.width}" height="${
												logo.height
											}" style="display: block; border: 0px" />
                    </td>
                  </tr>
                  <tr>
                    <td align="center" style="
                        font-family: Open Sans, Helvetica, Arial, sans-serif;
                        font-size: 30px;
                        font-weight: 800;
                        line-height: 36px;
                        padding-top: 19px;
                        color: #323232;
                      ">${labels.title}
                    </td>
                  </tr>
                  <tr>
                    <td align="left" style="
                        font-family: Open Sans, Helvetica, Arial, sans-serif;
                        font-size: 16px;
                        font-weight: 400;
                        line-height: 24px;
                        padding-top: 15px;
                        padding-bottom: 15px;
                        color: #afafaf;
                      ">${labels.subtitle}
                    </td>
                  </tr>
                  <tr>
                    <td align="left" style="padding-top: 20px">
                      <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                          <td colspan="2" width="75%" align="left" bgcolor="#eeeeee" style="
                              font-family: Open Sans, Helvetica, Arial, sans-serif;
                              font-size: 16px;
                              font-weight: 800;
                              line-height: 24px;
                              padding: 10px;
                              color: #323232;
                            ">${labels.orderConfirmation}</td>
                        </tr>
                        ${tableItems
													.map((tableItem) => {
														return `<tr>
                            <td width="50%" align="left" style="
                                font-family: Open Sans, Helvetica, Arial, sans-serif;
                                font-size: 16px;
                                font-weight: 400;
                                line-height: 24px;
                                padding: 15px 10px 5px 10px;
                                color: #323232;
                              "> ${tableItem.label} </td>
                            <td width="25%" align="left" style="
                                font-family: Open Sans, Helvetica, Arial, sans-serif;
                                font-size: 16px;
                                font-weight: 400;
                                line-height: 24px;
                                padding: 15px 10px 5px 10px;
                                color: #323232;
                              "> ${tableItem.size || ''} </td>
                            <td width="25%" align="left" style="
                                font-family: Open Sans, Helvetica, Arial, sans-serif;
                                font-size: 16px;
                                font-weight: 400;
                                line-height: 24px;
                                padding: 15px 10px 5px 10px;
                                color: #323232;
                              "> ${tableItem.price} </td>
                          </tr>`;
													})
													.join('\n')}
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="left" style="padding-top: 20px">
                      <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                          <td width="75%" align="left" style="
                              font-family: Open Sans, Helvetica, Arial, sans-serif;
                              font-size: 16px;
                              font-weight: 800;
                              line-height: 24px;
                              padding: 10px;
                              border-top: 3px solid #eeeeee;
                              border-bottom: 3px solid #eeeeee;
                              color: #323232;
                            ">${labels.total}</td>
                          <td width="25%" align="left" style="
                              font-family: Open Sans, Helvetica, Arial, sans-serif;
                              font-size: 16px;
                              font-weight: 800;
                              line-height: 24px;
                              padding: 10px;
                              border-top: 3px solid #eeeeee;
                              border-bottom: 3px solid #eeeeee;
                              color: #323232;
                            ">${totalPrice}</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" height="100%" valign="top" width="100%" style="padding: 0 35px 35px 35px; background-color: #ffffff" bgcolor="#ffffff">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 660px">
                  <tr>
                    <td align="left" valign="top" style="
                        font-family: Open Sans, Helvetica, Arial,
                            sans-serif;
                        font-size: 16px;
                        font-weight: 400;
                        line-height: 24px;
                        color: #323232;
                        ">
                        <p style="font-weight: 800">${labels.address}</p>
                        <p style="margin: 0">${name}</p>
                        <p style="margin: 0">${email}</p>
                        <p style="margin: 0">${address}</p>
                        <p style="margin: 0">${phoneNumber}</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" style="padding: 35px; background-color: #ffffff" bgcolor="#ffffff">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px">
                  <tr>
                    <td align="left" style="
                        font-family: Open Sans, Helvetica, Arial, sans-serif;
                        font-size: 14px;
                        font-weight: 400;
                        line-height: 24px;
                      ">
                      <p style="
                          font-size: 14px;
                          font-weight: 400;
                          line-height: 20px;
                          color: #afafaf;
                          text-align: center;
                        ">${labels.footer}
                      </p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>`;
};
