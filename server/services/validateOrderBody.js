const axios = require('axios');
const Joi = require('joi');
const { ERROR_CODE, STRAPI_LOCAL_API_URL } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

const orderSchema = Joi.object({
	firstName: Joi.string().min(1).max(255).required(),
	lastName: Joi.string().min(1).max(255).required(),
	email: Joi.string().email().required(),
	phoneNumber: Joi.string().min(0).max(255).required(),
	inpostOfficePoint: Joi.object().required(),
	promoCode: Joi.string().allow(null).min(0).max(255),
	products: Joi.array()
		.min(1)
		.items(
			Joi.object({
				id: Joi.number(),
				name: Joi.string(),
				price: Joi.number(),
				quantity: Joi.number(),
				sizeId: Joi.number(),
				sizeTitle: Joi.string(),
			}),
		)
		.required(),
	shippingPrice: Joi.number().required(),
	totalPrice: Joi.number().required(),
});

module.exports = async function validateOrderBody(body) {
	const { error: orderSchemaError } = await handlePromise(orderSchema.validateAsync(body));

	if (orderSchemaError) {
		return {
			error: {
				code: ERROR_CODE.ORDER_INVALID_FORM_FIELDS,
				data: orderSchemaError,
			},
		};
	}

	const { data: promoCodesResponse, error: promoCodesError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/promo-codes`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (promoCodesError) {
		return {
			error: {
				code: ERROR_CODE.UNKNOWN_ERROR,
				data: promoCodesError,
			},
		};
	}

	const promoCodes = promoCodesResponse.data.data;
	const promoCode = promoCodes.find(
		({ attributes }) => attributes.Code === body.promoCode && attributes.Count !== '0',
	);

	if (body.promoCode && !promoCode) {
		return {
			error: {
				code: ERROR_CODE.ORDER_INVALID_PROMO_CODE,
			},
		};
	}

	const { data: productsResponse, error: productsError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/products?populate=deep,3&pagination[pageSize]=1000`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (productsError) {
		return {
			error: {
				code: ERROR_CODE.UNKNOWN_ERROR,
				data: productsError,
			},
		};
	}

	const products = productsResponse.data.data;
	const invalidProduct = body.products.some((orderProduct) => {
		const product = products.find(
			(product) =>
				product.id === orderProduct.id && product.attributes.Price === orderProduct.price,
		);

		if (!product) {
			return true;
		}

		const targetProductSizeToQuantity = product.attributes.ProductSizeToQuantity.find((item) => {
			return item.product_size.data.id === orderProduct.sizeId;
		});

		return (
			orderProduct.quantity > 10 ||
			orderProduct.quantity < 1 ||
			orderProduct.quantity > parseInt(targetProductSizeToQuantity.Quantity)
		);
	});

	if (invalidProduct) {
		return {
			error: {
				code: ERROR_CODE.ORDER_INVALID_PRODUCTS,
				data: invalidProduct,
			},
		};
	}

	const { data: orderCheckoutPageResponse, error: orderCheckoutPageError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/order-checkout`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (orderCheckoutPageError) {
		return {
			error: {
				code: ERROR_CODE.UNKNOWN_ERROR,
				data: orderCheckoutPageError,
			},
		};
	}

	const orderCheckoutPageData = orderCheckoutPageResponse.data.data;
	const totalProductsPrice = body.products.reduce((total, product) => {
		return total + product.price * product.quantity;
	}, 0);
	const subTotalWithPromoCodePrice = promoCode
		? totalProductsPrice - (totalProductsPrice * promoCode.attributes.DiscountPercentage) / 100
		: totalProductsPrice;
	let shippingPrice = orderCheckoutPageData.attributes.ShippingPrice;

	if (subTotalWithPromoCodePrice >= orderCheckoutPageData.attributes.FreeShippingFrom) {
		shippingPrice = 0;
	}

	for (const product of body.products) {
		if (products.some(({ id, attributes }) => id === product.id && attributes.FreeShipping)) {
			shippingPrice = 0;
			break;
		}
	}

	if (body.shippingPrice !== shippingPrice) {
		return {
			error: {
				code: ERROR_CODE.ORDER_INVALID_SHIPPING_PRICE,
			},
		};
	}

	const totalPrice = subTotalWithPromoCodePrice + shippingPrice;

	if (totalPrice !== body.totalPrice) {
		return {
			error: {
				code: ERROR_CODE.ORDER_INVALID_TOTAL_PRICE,
			},
		};
	}

	return {};
};
