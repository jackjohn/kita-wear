const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function getStrapiProduct(productId) {
	const { data: productResponse, error: productResponseError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/products/${productId}?populate=deep,3`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (productResponseError) {
		return {
			error: {
				code: ERROR_CODE.FETCH_PRODUCT_ERROR,
				data: productResponseError,
			},
		};
	}

	return {
		data: productResponse.data.data,
	};
};
