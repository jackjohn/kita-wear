const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function getStrapiClientOrderConfirmedEmail() {
	const { data: clientOrderConfirmedEmailResponse, error: clientOrderConfirmedEmailResponseError } =
		await handlePromise(
			axios.get(`${STRAPI_LOCAL_API_URL}/client-order-confirmed-email`, {
				headers: {
					Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
				},
			}),
		);

	if (clientOrderConfirmedEmailResponseError) {
		return {
			error: {
				code: ERROR_CODE.FETCH_CLIENT_ORDER_CONFIRMED_EMAIL_ERROR,
			},
		};
	}

	return {
		data: clientOrderConfirmedEmailResponse.data.data,
	};
};
