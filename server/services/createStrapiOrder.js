const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ORDER_STATUS, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');
const getStrapiPromoCode = require('./getStrapiPromoCode');

module.exports = async function createStrapiOrder(orderReqBody, customerIp) {
	const {
		firstName,
		lastName,
		email,
		phoneNumber,
		inpostOfficePoint,
		promoCode: promoCodeStr,
		products,
		shippingPrice,
		totalPrice,
	} = orderReqBody;

	let promoCodeId = null;

	if (promoCodeStr) {
		const { data: promoCode, error: promoCodeError } = await getStrapiPromoCode(promoCodeStr);

		if (promoCodeError) {
			return {
				error: {
					code: ERROR_CODE.FETCH_PROMO_CODE_ERROR,
					data: promoCodeError,
				},
			};
		}

		promoCodeId = promoCode.id;
	}

	const { data: createdStrapiOrder, error: createdStrapiOrderError } = await handlePromise(
		axios.post(
			`${STRAPI_LOCAL_API_URL}/orders`,
			{
				data: {
					FirstName: firstName,
					LastName: lastName,
					Email: email,
					PhoneNumber: phoneNumber,
					InpostOfficePoint: inpostOfficePoint,
					promo_code: promoCodeId,
					Products: products,
					ShippingPrice: shippingPrice,
					TotalPrice: totalPrice,
					Status: ORDER_STATUS.CREATED,
					CustomerIP: customerIp,
					publishedAt: null,
				},
			},
			{
				headers: {
					Authorization: `bearer ${process.env.STRAPI_API_FULL_ACCESS_TOKEN}`,
				},
			},
		),
	).then((response) => response.data.data);

	if (createdStrapiOrderError) {
		return {
			error: {
				code: ERROR_CODE.STRAPI_ORDER_CREATE_ERROR,
				data: createdStrapiOrder,
			},
		};
	}

	return {
		data: createdStrapiOrder,
	};
};
