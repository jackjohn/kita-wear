const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function getStrapiPromoCode(targetPromoCode) {
	const { data: promoCodesResponse, error: promoCodesResponseError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/promo-codes`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (promoCodesResponseError) {
		return {
			error: {
				code: ERROR_CODE.FETCH_PROMO_CODES_ERROR,
				data: promoCodesResponseError,
			},
		};
	}

	const promoCodes = promoCodesResponse.data;

	const promoCode = promoCodes.data.find(
		({ attributes }) => attributes.Code === targetPromoCode && attributes.Count !== '0',
	);

	if (!promoCode) {
		return {
			error: {
				code: ERROR_CODE.PROMO_CODE_NOT_FOUND,
			},
		};
	}

	return {
		data: promoCode,
	};
};
