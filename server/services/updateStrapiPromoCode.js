const axios = require('axios');
const { STRAPI_LOCAL_API_URL } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function updateStrapiPromoCode(strapiPromoCodeId, data) {
	return handlePromise(
		axios.put(
			`${STRAPI_LOCAL_API_URL}/promo-codes/${strapiPromoCodeId}`,
			{
				data,
			},
			{
				headers: {
					Authorization: `bearer ${process.env.STRAPI_API_FULL_ACCESS_TOKEN}`,
				},
			},
		),
	);
};
