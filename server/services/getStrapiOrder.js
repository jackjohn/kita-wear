const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function getStrapiOrder(orderId) {
	const { data: orderResponse, error: orderResponseError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/orders/${orderId}?populate=*`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (orderResponseError) {
		return {
			error: {
				code: ERROR_CODE.FETCH_ORDER_ERROR,
				data: orderResponseError,
			},
		};
	}

	return {
		data: orderResponse.data.data,
	};
};
