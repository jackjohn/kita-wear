const sendEmail = require('../utils/sendEmail');
const getClientOrderEmailHTML = require('../emailTemplates/clientOrder');
const getStrapiClientOrderConfirmedEmail = require('../services/getStrapiClientOrderConfirmedEmail');
const markdownit = require('markdown-it')();

module.exports = async function sendOrderConfirmedEmails(strapiOrder, strapiProducts) {
	const { data: clientOrderConfirmedEmail, error: clientOrderConfirmedEmailError } =
		await getStrapiClientOrderConfirmedEmail();

	if (clientOrderConfirmedEmailError) {
		return;
	}

	const logo = {
		src: `${process.env.APP_BASE_URL}/images/emailLogo.png`,
		width: '125',
		height: '125',
	};

	const tableItems = strapiOrder.attributes.Products.map((orderProduct) => {
		const strapiProduct = strapiProducts.find(({ id }) => id === orderProduct.id);
		return {
			label: `${strapiProduct.attributes.Title} (${orderProduct.quantity})`,
			price: `${parseFloat(strapiProduct.attributes.Price) * orderProduct.quantity} PLN`,
			size: orderProduct.sizeTitle,
		};
	}).concat([
		{
			label: clientOrderConfirmedEmail.attributes.ShippingPrice,
			price: `${strapiOrder.attributes.ShippingPrice} PLN`,
		},
	]);

	if (strapiOrder.attributes.promo_code.data) {
		tableItems.push({
			label: clientOrderConfirmedEmail.attributes.Discount,
			price: `${strapiOrder.attributes.promo_code.data.attributes.DiscountPercentage}%`,
		});
	}

	const inpostOfficePoint = strapiOrder.attributes.InpostOfficePoint;
	const name = `${strapiOrder.attributes.FirstName} ${strapiOrder.attributes.LastName}`;
	const email = strapiOrder.attributes.Email;
	const address = `${inpostOfficePoint.name} (${inpostOfficePoint.address.line1}, ${inpostOfficePoint.address.line2})`;
	const phoneNumber = strapiOrder.attributes.PhoneNumber;

	sendEmail({
		targetEmail: strapiOrder.attributes.Email,
		subject: `${clientOrderConfirmedEmail.attributes.Subject.replace('$orderId', strapiOrder.id)}`,
		html: getClientOrderEmailHTML({
			tableItems,
			totalPrice: `${strapiOrder.attributes.TotalPrice} PLN`,
			name,
			email,
			address,
			phoneNumber,
			logo,
			labels: {
				title: clientOrderConfirmedEmail.attributes.Title.replace(
					'$firstName',
					strapiOrder.attributes.FirstName,
				).replace('$lastName', strapiOrder.attributes.LastName),
				subtitle: clientOrderConfirmedEmail.attributes.Subtitle.replace(
					'$firstName',
					strapiOrder.attributes.FirstName,
				).replace('$lastName', strapiOrder.attributes.LastName),
				orderConfirmation: clientOrderConfirmedEmail.attributes.OrderConfirmation.replace(
					'$orderId',
					strapiOrder.id,
				),
				total: clientOrderConfirmedEmail.attributes.Total,
				address: clientOrderConfirmedEmail.attributes.Address,
				footer: markdownit.render(clientOrderConfirmedEmail.attributes.Footer),
			},
		}),
	});

	clientOrderConfirmedEmail.attributes.CompanyEmails.split(',').forEach((companyEmail) => {
		sendEmail({
			targetEmail: companyEmail,
			subject: `${clientOrderConfirmedEmail.attributes.Subject.replace(
				'$orderId',
				strapiOrder.id,
			)}`,
			html: getClientOrderEmailHTML({
				orderId: strapiOrder.id,
				tableItems,
				totalPrice: `${strapiOrder.attributes.TotalPrice} PLN`,
				name,
				email,
				address,
				phoneNumber,
				logo,
				labels: {
					title: ``,
					subtitle: '',
					orderConfirmation: clientOrderConfirmedEmail.attributes.OrderConfirmation.replace(
						'$orderId',
						strapiOrder.id,
					),
					total: clientOrderConfirmedEmail.attributes.Total,
					address: clientOrderConfirmedEmail.attributes.Address,
					footer: '',
				},
			}),
		});
	});
};
