const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function updateStrapiOrder(strapiOrderId, data) {
	const { error } = await handlePromise(
		axios.put(
			`${STRAPI_LOCAL_API_URL}/orders/${strapiOrderId}`,
			{
				data,
			},
			{
				headers: {
					Authorization: `bearer ${process.env.STRAPI_API_FULL_ACCESS_TOKEN}`,
				},
			},
		),
	);

	if (error) {
		return {
			error: {
				code: ERROR_CODE.STRAPI_ORDER_UPDATE_ERROR,
				data: error,
			},
		};
	}

	return {};
};
