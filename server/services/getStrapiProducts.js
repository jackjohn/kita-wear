const axios = require('axios');
const { STRAPI_LOCAL_API_URL, ERROR_CODE } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function getStrapiProducts() {
	const { data: productsResponse, error: productsResponseError } = await handlePromise(
		axios.get(`${STRAPI_LOCAL_API_URL}/products?pagination[pageSize]=1000`, {
			headers: {
				Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
			},
		}),
	);

	if (productsResponseError) {
		return {
			error: {
				code: ERROR_CODE.FETCH_PRODUCTS_ERROR,
				data: productsResponseError,
			},
		};
	}

	return {
		data: productsResponse.data.data,
	};
};
