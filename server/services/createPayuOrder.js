const axios = require('axios');
const { ERROR_CODE, PAYU_VALIDITY_TIME } = require('../utils/constants');
const handlePromise = require('../utils/handlePromise');

module.exports = async function createPayuOrder(strapiOrder) {
	const { data: token, error: tokenError } = await getPayuToken();

	if (tokenError) {
		return {
			error: tokenError,
		};
	}

	const headers = {
		'Content-Type': 'application/json',
		Authorization: 'Bearer ' + token,
	};
	const options = { headers, maxRedirects: 0 };
	const body = {
		customerIp: strapiOrder.attributes.CustomerIP,
		merchantPosId: process.env.PAYU_CLIENT_ID,
		extOrderId: strapiOrder.id,
		totalAmount: strapiOrder.attributes.TotalPrice * 100,
		description: 'Kita',
		currencyCode: 'PLN',
		validityTime: PAYU_VALIDITY_TIME,
		products: strapiOrder.attributes.Products.map((product) => ({
			name: product.name,
			unitPrice: `${product.price}`,
			quantity: `${product.quantity}`,
		})),
		notifyUrl: `${process.env.APP_BASE_URL}/payu-webhook`,
		continueUrl:
			process.env.NODE_ENV === 'development'
				? `http://127.0.0.1:9000/order/status?orderId=${strapiOrder.id}`
				: `${process.env.APP_BASE_URL}/order/status?orderId=${strapiOrder.id}`,
	};
	// The post wants to redirect the user to the payment page
	// since we've set the `maxRedirects` to 0, an error will be thrown
	const { error: payuOrderError } = await handlePromise(
		axios.post(`${process.env.PAYU_BASE_URL}/api/v2_1/orders`, body, options),
	);

	if (
		payuOrderError.response &&
		payuOrderError.response.data &&
		payuOrderError.response.data.status &&
		payuOrderError.response.data.status.statusCode === 'SUCCESS'
	) {
		return {
			data: payuOrderError.response.data,
		};
	} else if (payuOrderError) {
		return {
			error: {
				code: ERROR_CODE.PAYU_ORDER_CREATE_ERROR,
				data: payuOrderError.response,
			},
		};
	}
};

async function getPayuToken() {
	const params = new URLSearchParams();
	params.append('grant_type', 'client_credentials');
	params.append('client_id', process.env.PAYU_CLIENT_ID);
	params.append('client_secret', process.env.PAYU_CLIENT_SECRET);

	const headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
	};

	const { data: res, error } = await handlePromise(
		axios.post(`${process.env.PAYU_BASE_URL}/pl/standard/user/oauth/authorize`, params, {
			headers,
		}),
	);

	if (error || !res.data?.access_token) {
		return {
			error: ERROR_CODE.PAYU_INVALID_ACCESS_TOKEN,
		};
	}

	return {
		data: res.data.access_token,
	};
}
