require('dotenv').config();
const path = require('path');
const express = require('express');
const app = express();
const port = process.env.PORT;
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const winston = require('winston');
const expressWinston = require('express-winston');
const setHeadersForStatic = require('./utils/setStaticFilesHeaders');

const { strapiProxy } = require('./routes/strapiProxy');
const { getPromoCode } = require('./routes/promoCode');
const { getOrderStatus, postOrder } = require('./routes/order');
const { payuWebhook } = require('./routes/payuWebhook');
const { deployStrapiFrontend } = require('./routes/deployStrapiFrontend');

if (process.env.NODE_ENV === 'production') {
	app.use(compression());
	app.set('trust proxy', true);
}

app.use(
	expressWinston.logger({
		transports: [new winston.transports.Console()],
		format: winston.format.combine(winston.format.colorize(), winston.format.json()),
		expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
		colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
	}),
);
expressWinston.requestWhitelist.push('body');

app.use(
	expressWinston.errorLogger({
		transports: [new winston.transports.Console()],
		format: winston.format.combine(winston.format.colorize(), winston.format.json({ space: 2 })),
	}),
);
app.use(
	bodyParser.json({
		verify: function (req, res, buf) {
			req.rawBody = buf.toString();
		},
	}),
);
app.use(cookieParser());

if (process.env.NODE_ENV === 'development') {
	const cors = require('cors');
	const corsOptionsDelegate = function (req, callback) {
		callback(null, { origin: true });
	};

	app.use(cors(corsOptionsDelegate));
}

app.use('/strapi', strapiProxy);
app.get('/api/promo-code/:code', getPromoCode);
app.post('/api/order', postOrder);
app.get('/api/order-status/:id', getOrderStatus);
app.post('/payu-webhook', payuWebhook);
app.post('/strapi-deploy', deployStrapiFrontend);

app.use((req, res, ...rest) => {
	if (req.cookies && req.cookies.preview === 'true') {
		express.static('./frontend/.output-preview/public')(req, res, ...rest);
	} else {
		express.static('./frontend/.output/public', {
			setHeaders: setHeadersForStatic,
		})(req, res, ...rest);
	}
});

app.all('*', (_, res) => {
	res.sendFile('./frontend/.output/public/404.html', { root: '.' });
});

app.listen(port, () => {
	console.log(`App listening on port ${port}`);
});
