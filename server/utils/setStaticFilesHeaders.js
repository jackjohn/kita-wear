const path = require('path');

const pathsToCache = ['public/_nuxt', 'public/uploads'];

module.exports = function setHeadersForStatic(res, pathRequested) {
	if (pathsToCache.some((pathToCache) => pathRequested.includes(pathToCache))) {
		res.setHeader('Cache-Control', 'public, max-age=31536000');
	} else {
		res.setHeader('Cache-Control', 'public, max-age=0');
	}
};
