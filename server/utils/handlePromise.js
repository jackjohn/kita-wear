module.exports = function handlePromise(promise) {
	return promise.then((data) => ({ data })).catch((error) => ({ error }));
};
