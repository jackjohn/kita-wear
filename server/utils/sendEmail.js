const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: process.env.AUTO_EMAIL,
		pass: process.env.AUTO_EMAIL_PASSWORD,
	},
});

module.exports = function sendEmail({ targetEmail, subject, html }) {
	const mailOptions = {
		from: process.env.AUTO_EMAIL,
		to: targetEmail,
		subject,
		html,
	};

	return new Promise((resolve, reject) => {
		transporter.sendMail(mailOptions, function (err, info) {
			if (err) {
				reject(err);
			} else {
				resolve(info);
			}
		});
	});
};
