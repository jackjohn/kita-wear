const STRAPI_LOCAL_API_URL = `http://${
	process.env.NODE_ENV === 'production' ? '127.0.0.1' : 'localhost'
}:1337/api`;

const ERROR_CODE = {
	ORDER_INVALID_FORM_FIELDS: 'orderInvalidFormFields',
	ORDER_INVALID_PROMO_CODE: 'orderInvalidPromoCode',
	ORDER_INVALID_PRODUCTS: 'orderInvalidProducts',
	ORDER_INVALID_SHIPPING_PRICE: 'orderInvalidShippingPrice',
	ORDER_INVALID_TOTAL_PRICE: 'orderInvalidTotalPrice',
	STRAPI_ORDER_CREATE_ERROR: 'strapiOrderCreateError',
	STRAPI_ORDER_UPDATE_ERROR: 'strapiOrderUpdateError',
	STRAPI_PRODUCT_UPDATE_ERROR: 'strapiProductUpdateError',
	PAYU_INVALID_ACCESS_TOKEN: 'payuInvalidAccessToken',
	PAYU_ORDER_CREATE_ERROR: 'payuOrderCreate',
	FETCH_PROMO_CODES_ERROR: 'fetchPromoCodesError',
	FETCH_PROMO_CODE_ERROR: 'fetchPromoCodeError',
	FETCH_PRODUCT_ERROR: 'fetchProductError',
	FETCH_PRODUCTS_ERROR: 'fetchProductsError',
	FETCH_ORDER_ERROR: 'fetchOrderError',
	FETCH_CLIENT_ORDER_CONFIRMED_EMAIL_ERROR: 'fetchClientOrderConfirmedEmailError',
	PROMO_CODE_NOT_FOUND: 'promoCodeNotFound',
	UNKNOWN_ERROR: 'unknownError',
};

const ORDER_STATUS = {
	NEW: 'NEW',
	CREATED: 'CREATED',
	PENDING: 'PENDING',
	WAITING_FOR_CONFIRMATION: 'WAITING_FOR_CONFIRMATION',
	COMPLETED: 'COMPLETED',
	CANCELED: 'CANCELED',
};

module.exports = {
	STRAPI_LOCAL_API_URL,
	ERROR_CODE,
	PAYU_VALIDITY_TIME: 5 * 60, // in seconds
	ORDER_STATUS,
};
