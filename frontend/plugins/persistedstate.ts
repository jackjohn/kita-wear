import { createPersistedStatePlugin } from 'pinia-plugin-persistedstate-2';
import { defineNuxtPlugin } from '#imports';

export default defineNuxtPlugin((nuxtApp: any) => {
	if (!process.server) {
		nuxtApp.$pinia.use(createPersistedStatePlugin());
	}
});
