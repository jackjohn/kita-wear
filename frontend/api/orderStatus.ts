import { RuntimeConfig } from 'nuxt/schema';

enum OrderStatus {
	NEW = 'NEW',
	PENDING = 'PENDING',
	COMPLETED = 'COMPLETED',
}

export function getOrderStatus(runtimeConfig: RuntimeConfig, orderId: string) {
	return $fetch<{ status: OrderStatus }>(
		`${runtimeConfig.public.APP_BASE_URL}/api/order-status/${orderId}`,
	);
}
