import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	MenCollection: string;
	WomenCollection: string;
	Seo: PageSeo;
}>;

export function useGetProductsPage() {
	const params = useFetchParams(`/strapi/api/products-page?populate=deep,3`, {
		key: 'products-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
