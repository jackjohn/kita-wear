import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	ColorLabel: string;
	PriceLabel: string;
	ProductDescriptionLabel: string;
	ModelParametersLabel: string;
	CompositionAndCareLabel: string;
	ShippingAndPaymentLabel: string;
	ShippingAndPaymentContent: string;
	RelatedProductsLabel: string;
	RelatedProductsSublabel: string;
	AddToCartLabel: string;
	OneClickBuyLabel: string;
	ProductAddedToCartMessage: string;
	SelectSizeLabel: string;
	Seo: PageSeo;
}>;

export type ProductPage = Data['data']['attributes'];

export function useGetProductPage() {
	const params = useFetchParams(`/strapi/api/product-page?populate=deep,3`, {
		key: 'product-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
