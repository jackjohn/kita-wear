import { computed, UseFetchOptions, useRuntimeConfig } from '#imports';
import { RuntimeConfig } from 'nuxt/schema';
import { ComputedRef, Ref } from 'vue';

export function getStrapiApiOptions(runtimeConfig: RuntimeConfig) {
	return {
		headers: {
			Authorization: `bearer ${runtimeConfig.public.STRAPI_API_TOKEN}`,
		},
	};
}

export function useFetchParams<ResponseData, ParsedData>(
	path: string | ComputedRef<string> | Ref<string>,
	options?: UseFetchOptions<ResponseData, ParsedData>,
) {
	const runtimeConfig = useRuntimeConfig();
	const url = computed(
		() => `${runtimeConfig.public.APP_BASE_URL}${typeof path === 'string' ? path : path.value}`,
	);
	return {
		url,
		options: {
			...getStrapiApiOptions(runtimeConfig),
			...options,
			watch: [url],
		},
	};
}
