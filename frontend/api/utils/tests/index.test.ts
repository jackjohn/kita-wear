import { describe, expect, test } from 'vitest';
import { getStrapiApiOptions, useFetchParams } from '#app/api/utils';
import { mockRuntimeConfig } from '#app/utils/tests/mocks';
import { ref } from '#imports';

describe('getStrapiApiOptions', () => {
	test('should return correct headers', () => {
		expect(getStrapiApiOptions(mockRuntimeConfig)).toMatchObject({
			headers: {
				Authorization: 'bearer mockedStrapiApiToken',
			},
		});
	});
});

describe('useFetchParams', () => {
	test('should return correct URL and options with a string path', () => {
		const fetchOptions = useFetchParams('/example/path');
		expect(fetchOptions.url.value).toBe('https://mockurl.com/example/path');
		expect(fetchOptions.options.headers).toMatchObject({
			Authorization: 'bearer mockedStrapiApiToken',
		});
	});

	test('should return correct URL and options with a ComputedRef path', () => {
		const mockPath = ref('/example/path');
		const fetchOptions = useFetchParams(mockPath);
		expect(fetchOptions.url.value).toBe('https://mockurl.com/example/path');
		expect(fetchOptions.options.headers).toMatchObject({
			Authorization: 'bearer mockedStrapiApiToken',
		});

		mockPath.value = '/modified/path';

		expect(fetchOptions.url.value).toBe('https://mockurl.com/modified/path');
		expect(fetchOptions.options.headers).toMatchObject({
			Authorization: 'bearer mockedStrapiApiToken',
		});
	});
});
