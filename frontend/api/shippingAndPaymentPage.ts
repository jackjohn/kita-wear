import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	Content: string;
	Seo: PageSeo;
}>;

export function useGetShippingAndPaymentPage() {
	const params = useFetchParams(`/strapi/api/shipping-and-payment-page?populate=deep,3`, {
		key: 'shipping-and-payment-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
