import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	ProductDetailsLabel: string;
	PriceLabel: string;
	QuantityLabel: string;
	ColorLabel: string;
	SizeLabel: string;
	SubtotalLabel: string;
	FreeShippingLabel: string;
	FreeShippingFrom: number;
	DeliveryTitle: string;
	NameLabel: string;
	SurnameLabel: string;
	PhoneNumberLabel: string;
	EmailLabel: string;
	InpostOfficeLabel: string;
	TotalLabel: string;
	PromoCodePlaceholder: string;
	PromoCodeLabel: string;
	ShippingLabel: string;
	PayLabel: string;
	EmptyCartLabel: string;
	FreeShippingDescription: string;
	ShippingPrice: number;
	PromoCodeValidNotification: string;
	PromoCodeInvalidNotification: string;
	InpostLoadError: string;
	SubmitError: string;
	PrivacyAgreementLabel: string;
	Seo: PageSeo;
}>;

export type OrderCheckoutPage = Data['data']['attributes'];

export function useGetOrderCheckoutPage() {
	const params = useFetchParams(`/strapi/api/order-checkout?populate=deep,3`, {
		key: `order-checkout-page-${Math.random()}`,
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
