import { useFetch } from '#imports';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	PublicOfferAgreement: string;
	PrivacyPolicy: string;
	ShippingAndPayment: string;
	ClothingCare: string;
	ReturnAndExchange: string;
	ContactUs: string;
	InstagramTitle: string;
	InstagramLink: string;
	FacebookTitle: string;
	FacebookLink: string;
	About: string;
}>;

export function useGetFooter() {
	const params = useFetchParams(`/strapi/api/footer`, {
		key: 'footer',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
