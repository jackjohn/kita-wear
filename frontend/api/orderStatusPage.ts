import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	PendingTitle: string;
	PendingSubtitle: string;
	WaitingForConfirmationTitle: string;
	WaitingForConfirmationSubtitle: string;
	CompletedTitle: string;
	CompletedSubtitle: string;
	CancelledTitle: string;
	CancelledSubtitle: string;
	NotFoundTitle: string;
	NotFoundSubtitle: string;
	Seo: PageSeo;
}>;

export function useGetOrderStatusPage() {
	const params = useFetchParams(`/strapi/api/order-status-page?populate=deep,3`, {
		key: 'order-status-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
