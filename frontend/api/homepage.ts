import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { ImageResource } from './types/ImageResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	ForHim: string;
	ForHer: string;
	LeftPhoto: ImageResource;
	RightPhoto: ImageResource;
	Seo: PageSeo;
}>;

export function useGetHomepage() {
	const params = useFetchParams(`/strapi/api/homepage?populate=deep,3`, {
		key: 'homepage',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
