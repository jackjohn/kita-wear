import { OrderPayload } from '@app/types/OrderPayload';
import { RuntimeConfig } from 'nuxt/schema';

export async function createOrder(runtimeConfig: RuntimeConfig, payload: OrderPayload) {
	return await $fetch<{ payuRedirectUrl: string }>(
		`${runtimeConfig.public.APP_BASE_URL}/api/order`,
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(payload),
		},
	);
}
