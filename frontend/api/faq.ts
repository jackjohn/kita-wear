import { useFetch } from '#imports';
import { ApiResources } from './types/ApiResources';
import { useFetchParams } from './utils';

type Data = ApiResources<{
	Title: string;
	Description: string;
}>;

export function useGetFaqs() {
	const params = useFetchParams('/strapi/api/faqs', {
		key: 'faqs',
		transform: (data: unknown) => (data as Data).data,
	});
	return useFetch(params.url, params.options);
}
