import { PromoCode } from '@app/api/types/PromoCode';
import { RuntimeConfig } from 'nuxt/schema';

export function getPromoCode(runtimeConfig: RuntimeConfig, code: string) {
	return $fetch<PromoCode>(`${runtimeConfig.public.APP_BASE_URL}/api/promo-code/${code}`);
}
