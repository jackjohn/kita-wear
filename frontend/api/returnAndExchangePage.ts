import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	Content: string;
	Seo: PageSeo;
}>;

export function useGetReturnAndExchangePage() {
	const params = useFetchParams(`/strapi/api/return-and-exchange-page?populate=deep,3`, {
		key: 'return-and-exchange-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
