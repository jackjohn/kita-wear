import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { ImageResource } from './types/ImageResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	StartingDate: string;
	StartingDescription: string;
	QualityTitle: string;
	QualityDescription: string;
	Image: ImageResource;
	Video: {
		data: {
			id: string;
			attributes: {
				url: string;
			};
		};
	};
	Seo: PageSeo;
}>;

export function useGetAboutPage() {
	const params = useFetchParams('/strapi/api/about?populate=deep,3', {
		key: 'about',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
