import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	Content: string;
	Seo: PageSeo;
}>;

export type ContactUsPage = Data['data']['attributes'];

export function useGetContactUsPage() {
	const params = useFetchParams('/strapi/api/contact-us-page?populate=deep,3', {
		key: 'contact-us-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
