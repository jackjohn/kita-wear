import { ImageResource } from './ImageResource';

export interface PageSeo {
	Title: string;
	Description: string;
	Keywords: string;
	Image: ImageResource;
	PreventIndexing: boolean;
}
