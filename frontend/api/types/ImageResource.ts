export enum ImageFormat {
	XXSMALL = 'xxsmall',
	XSMALL = 'xsmall',
	SMALL = 'small',
	MEDIUM = 'medium',
	LARGE = 'large',
	XLARGE = 'xlarge',
	THUMBNAIL = 'thumbnail',
}

interface BaseImageResource {
	id: number;
	attributes: {
		name: string;
		url: string;
		alternativeText?: string | null;
		caption?: string | null;
		width?: number;
		height?: number;
		hash?: string;
		ext?: string;
		mime?: string;
		size?: number;
		previewUrl?: string | null;
		provider?: string;
		provider_metadata?: {
			public_id: string;
			resource_type: string;
		} | null;
		createdAt?: string;
		updatedAt?: string;
		formats: {
			[key in ImageFormat]?: {
				ext: string;
				hash: string;
				mime: string;
				path: string | null;
				size: number;
				width: number;
				height: number;
				provider_metadata?: {
					public_id: string;
					resource_type: string;
				} | null;
				name: string;
				url: string;
			};
		};
	};
}

export interface ImageResource<IsArray extends boolean = false> {
	data: IsArray extends true ? BaseImageResource[] : BaseImageResource;
}
