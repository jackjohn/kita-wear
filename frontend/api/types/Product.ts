import { ApiResource } from './ApiResource';
import { ApiResources } from './ApiResources';
import { ImageResource } from './ImageResource';

export interface Product {
	Title: string;
	Description: string;
	ModelParameters: string;
	CompositionAndCare: string;
	Price: number;
	MainPhoto?: ImageResource;
	BackgroundPhoto?: ImageResource;
	Photos?: ImageResource<true>;
	Color: string;
	Slug: string;
	FreeShipping: boolean;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	locale: string;
	product_type?: ApiResource<{
		Name: string;
		Slug: string;
		createdAt: string;
		updatedAt: string;
		publishedAt: string;
		locale: string;
		products?: ApiResources<Product>;
	}>;
	ProductSizeToQuantity?: Array<{
		Quantity: string;
		id: number;
		product_size?: ApiResource<{
			Title: string;
			Slug: string;
			createdAt: string;
			updatedAt: string;
			publishedAt: string;
		}>;
	}>;
	product_gender?: ApiResource<{
		Name: string;
		createdAt: string;
		updatedAt: string;
		publishedAt: string;
		locale: string;
		Slug: string;
	}>;

	RelatedProducts?: ApiResources<Product>;
}
