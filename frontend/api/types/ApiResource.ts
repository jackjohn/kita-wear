export interface ApiResource<Attributes> {
	data: {
		id: number;
		attributes: Attributes;
	};
}
