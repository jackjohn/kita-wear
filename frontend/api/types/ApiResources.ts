export interface ApiResources<Attributes> {
	data: {
		id: number;
		attributes: Attributes;
	}[];
}
