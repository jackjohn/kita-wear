import { useFetch } from '#imports';
import { ApiResources } from './types/ApiResources';
import { useFetchParams } from './utils';

type Data = ApiResources<{
	Name: string;
	Slug: string;
}>;

export type ProductType = Data['data'][0];

export function useGetProductTypes(genderSlug: string) {
	const params = useFetchParams(
		`/strapi/api/product-types?filters[products][product_gender][Slug][$in][0]=unisex&filters[products][product_gender][Slug][$in][1]=${genderSlug}`,
		{
			key: `product-types-${genderSlug}`,
			transform: (data: unknown) => (data as Data).data,
		},
	);
	return useFetch(params.url, params.options);
}
