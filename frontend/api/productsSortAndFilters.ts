import { useFetch } from '#imports';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	SortLabel: string;
	SortLastUpdatedLabel: string;
	SortHighPriceLabel: string;
	SortLowPriceLabel: string;
	OnlyInStockLabel: string;
}>;

export type ProductsSortAndFilters = Data['data']['attributes'];

export function useGetProductsSortAndFilters() {
	const params = useFetchParams(`/strapi/api/products-sort-and-filter`, {
		key: 'products-sort-and-filter',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
