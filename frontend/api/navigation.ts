import { useFetch } from '#imports';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	ProductsMenTitle: string;
	ProductsWomenTitle: string;
	AboutTitle: string;
	CenterText: string;
	InstagramTitle: string;
	InstagramLink: string;
	FacebookTitle: string;
	FacebookLink: string;
}>;

export function useGetNavigation() {
	const params = useFetchParams(`/strapi/api/navigation`, {
		key: 'navigation',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
