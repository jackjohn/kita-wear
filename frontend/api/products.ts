import qs from 'qs';
import { computed, useFetch, UseFetchOptions } from '#imports';
import { ApiResources } from './types/ApiResources';
import { useFetchParams } from './utils';
import { Product } from './types/Product';
import { ComputedRef } from 'vue';

type Data = ApiResources<Product>;

export type Products = Data['data'];

export function useGetProducts(
	filtersRef?: ComputedRef<{
		productTypeId?: number;
		productGenderSlug?: string;
		productIds?: number[];
		productSort?: 'new' | 'high_price' | 'low_price';
		productOnlyInStock?: boolean;
	}>,
	options?: UseFetchOptions<Data, Data['data']>,
) {
	const contextId = computed(() => {
		return [
			'products',
			filtersRef?.value?.productTypeId,
			filtersRef?.value?.productGenderSlug,
			filtersRef?.value?.productSort,
			filtersRef?.value?.productOnlyInStock,
		].join('-');
	});

	const url = computed(() => {
		const queryObj: any = {
			populate: 'deep,3',
			filters: {},
			pagination: {
				pageSize: 1000,
			},
		};

		const filters = filtersRef?.value;

		if (filters?.productTypeId) {
			queryObj.filters.product_type = {
				id: {
					$eq: filters.productTypeId,
				},
			};
		}
		if (filters?.productGenderSlug) {
			queryObj.filters.product_gender = {
				Slug: {
					$in: [filters.productGenderSlug, 'unisex'],
				},
			};
		}

		if (filters?.productIds) {
			queryObj.filters.id = {
				$in: filters.productIds,
			};
		}

		if (filters?.productSort === 'new') {
			queryObj.sort = 'updatedAt:desc';
		} else if (filters?.productSort === 'high_price') {
			queryObj.sort = 'Price:desc';
		} else if (filters?.productSort === 'low_price') {
			queryObj.sort = 'Price:asc';
		}

		if (filters?.productOnlyInStock) {
			queryObj.filters.ProductSizeToQuantity = {
				Quantity: {
					$ne: '0',
				},
			};
		}

		const queryStr = qs.stringify(queryObj, {
			encodeValuesOnly: true,
		});

		return `/strapi/api/products?${queryStr}`;
	});

	const params = useFetchParams(url, {
		key: contextId.value,
		transform: (data: unknown) => (data as Data).data,
		...options,
	});
	return useFetch(params.url, {
		...params.options,
		watch: [...params.options.watch, contextId, url],
	});
}
