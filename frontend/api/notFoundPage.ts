import { useFetch } from '#imports';
import { PageSeo } from './types/PageSeo';
import { ApiResource } from './types/ApiResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	SomethingWrongLabel: string;
	BackToHomePageLabel: string;
	Seo: PageSeo;
}>;

export function useGetNotFoundPage() {
	const params = useFetchParams(`/strapi/api/not-found-page?populate=deep,3`, {
		key: 'not-found-page',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
