import { useFetch } from '#imports';
import { ApiResource } from './types/ApiResource';
import { ImageResource } from './types/ImageResource';
import { useFetchParams } from './utils';

type Data = ApiResource<{
	Title: string;
	LinkTitle: string;
	Link: string;
	TopLeftImage: ImageResource;
	BottomLeftImage: ImageResource;
	TopRightImage: ImageResource;
	BottomRightImage: ImageResource;
	CenterImage: ImageResource;
}>;

export function useGetOurInstaPage() {
	const params = useFetchParams(`/strapi/api/our-insta?populate=*`, {
		key: 'our-insta',
		transform: (data: unknown) => (data as Data).data.attributes,
	});
	return useFetch(params.url, params.options);
}
