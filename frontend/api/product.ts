import { useFetch } from '#imports';
import { ApiResources } from './types/ApiResources';
import { Product as BaseProduct } from './types/Product';
import { useFetchParams } from './utils';

type Data = ApiResources<BaseProduct>;

export type Product = Data['data'][0];

function useGetFetchParams(productSlug: string) {
	return useFetchParams(`/strapi/api/products?populate=deep,3&filters[Slug][$eq]=${productSlug}`, {
		key: `product-${productSlug}`,
		transform: (data: unknown) => (data as Data).data[0],
	});
}

export function useGenerateGetProduct(productSlug: string) {
	const params = useGetFetchParams(productSlug);
	return () =>
		$fetch<Data>(params.url.value, {
			headers: params.options.headers as HeadersInit,
		}).then((data) => (data as Data).data[0]);
}

export function useGetProduct(productSlug: string) {
	const params = useGetFetchParams(productSlug);
	return useFetch(params.url, params.options);
}
