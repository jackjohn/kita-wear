import { InpostOfficePoint } from '@app/types/InpostOfficePoint';

export interface OrderPayload {
	firstName: string;
	lastName: string;
	email: string;
	phoneNumber: string;
	inpostOfficePoint: InpostOfficePoint;
	promoCode: string | null;
	products: Array<{
		id: number;
		price: number;
		quantity: number;
		sizeId: number;
		name: string;
		sizeTitle: string;
	}>;
	shippingPrice: number;
	totalPrice: number;
}
