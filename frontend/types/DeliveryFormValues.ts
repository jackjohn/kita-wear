import { InpostOfficePoint } from '@app/types/InpostOfficePoint';

export interface DeliveryFormValues {
	firstName: string;
	lastName: string;
	phoneNumber: string;
	email: string;
	inpostOffice: InpostOfficePoint | null;
	privacyAgreement: boolean;
}
