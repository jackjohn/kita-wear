import { ApiResource } from '#app/api/types/ApiResource';
import { Product } from '#app/api/types/Product';

export interface CartProduct {
	id: number;
	quantity: number;
	sizeId: number;
}

export type ProductFromCart = ApiResource<Product>['data'] & {
	cartInfo: CartProduct;
	disabled: boolean;
};
