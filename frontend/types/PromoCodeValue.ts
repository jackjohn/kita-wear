export interface PromoCodeValue {
	code: string;
	percentage: number;
}
