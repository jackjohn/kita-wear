import { DefineStoreOptions as DefaultDefineStoreOptions, StateTree } from 'pinia';

declare module 'pinia' {
	export interface DefineStoreOptions<Id extends string, S extends StateTree, G, A>
		extends DefaultDefineStoreOptions<Id, S, G, A> {
		persist?: boolean;
	}
}
