export interface InpostOfficePoint {
	address: {
		line1: string;
		line2: string;
	};
	address_details: {
		city: string;
		street: string;
		province: string;
		post_code: string;
		flat_number: number | null;
		building_number: string;
	};
	agency: string;
	air_index_level: string;
	apm_doubled: unknown;
	distance: number;
	easy_access_zone: boolean;
	functions: string[];
	href: string;
	image_url: string;
	is_next: boolean;
	location: {
		latitude: number;
		longitude: number;
	};
	location_247: boolean;
	location_date: string | null;
	location_description: string;
	location_description_1: string | null;
	location_description_2: string | null;
	location_type: string;
	name: string;
	opening_hours: string;
	operating_hours_extended: unknown;
	partner_id: number;
	payment_available: boolean;
	payment_point_descr: string;
	payment_type: Record<string, string>;
	phone_number: number | null;
	physical_type_description: string | null;
	physical_type_mapped: string;
	recommended_low_interest_box_machines_list: string[];
	status: string;
	type: string[];
	virtual: string;
}
