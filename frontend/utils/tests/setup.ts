import { beforeAll, vi } from 'vitest';
import { mockRuntimeConfig } from './mocks';
import { setup } from '@nuxt/test-utils';

beforeAll(async () => {
	await setup({
		fixture: '/Users/john/Projects/kita-wear/frontend',
		nuxtConfig: {
			alias: {
				'@app': '/Users/john/Projects/kita-wear/frontend',
				'@app/*': '/Users/john/Projects/kita-wear/frontend/*',
			},
			vite: {
				css: {
					preprocessorOptions: {
						scss: {
							additionalData:
								'@use "@/assets/styles/_variables.scss" as *; @use "@/assets/styles/typography.scss" as *;',
						},
					},
				},
			},
		},
	});
});

vi.mock('#imports', async (importOriginal) => {
	return {
		...((await importOriginal()) as any),
		useRuntimeConfig() {
			return mockRuntimeConfig;
		},
	};
});
