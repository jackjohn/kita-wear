export const mockRuntimeConfig = {
	app: {
		baseURL: 'https://mockurl.com',
		buildAssetsDir: 'mockedBuildAssetsDir',
		cdnURL: 'mockedCdnURL',
	},
	'nuxt-simple-sitemap': {} as any,
	public: {
		APP_BASE_URL: 'https://mockurl.com',
		STRAPI_API_TOKEN: 'mockedStrapiApiToken',
		INPOST_TOKEN: 'mockedInpostToken',
		siteUrl: 'mockedSiteUrl',
	},
};
