import { mockProducts } from './products';

export const mockCartProducts = [
	{
		id: mockProducts[0].id,
		quantity: 1,
		sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
	},
	{
		id: 9999,
		quantity: 2,
		sizeId: 99999,
	},
];
