import { useHead } from '#imports';
import { PageSeo } from '@app/api/types/PageSeo';
import { getImageUrl } from './image';

export function usePageSeo(data?: PageSeo) {
	let seoData = {};
	if (data) {
		seoData = {
			title: data.Title,
			meta: [
				{
					name: 'description',
					content: data.Description,
				},
				{
					name: 'keywords',
					content: data.Keywords,
				},
				{
					name: 'og:title',
					content: data.Title,
				},
				{
					name: 'og:description',
					content: data.Description,
				},
				{
					name: 'og:image',
					content: getImageUrl(data.Image),
				},
				...(data.PreventIndexing
					? [
							{
								name: 'robots',
								content: 'noindex,nofollow',
							},
							{
								name: 'googlebot',
								content: 'noindex,nofollow',
							},
					  ]
					: []),
			],
		};
	}

	useHead({
		...seoData,
		htmlAttrs: {
			lang: 'pl',
		},
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
	});
}
