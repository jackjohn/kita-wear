import { Product } from '@app/api/types/Product';

export function getProductSizes(product: Product) {
	return product.ProductSizeToQuantity.map((item) => item.product_size.data.attributes.Title).join(
		', ',
	);
}

export function getProductSizeTitle(product: Product, sizeId: number) {
	return product.ProductSizeToQuantity.find((item) => item.product_size.data.id === sizeId)
		?.product_size.data.attributes.Title;
}
