import { ImageFormat, ImageResource } from '@app/api/types/ImageResource';

const breakpoints = {
	[ImageFormat.XLARGE]: 1600,
	[ImageFormat.LARGE]: 1200,
	[ImageFormat.MEDIUM]: 992,
	[ImageFormat.SMALL]: 700,
	[ImageFormat.XSMALL]: 500,
	[ImageFormat.XXSMALL]: 300,
	[ImageFormat.THUMBNAIL]: null,
};

export function getImageUrl(imageResource: ImageResource, format?: ImageFormat) {
	if (!imageResource.data) {
		return '';
	}

	if (format) {
		const { formats } = imageResource.data.attributes;

		switch (format) {
			case ImageFormat.XXSMALL:
				return formats.xxsmall?.url;
			case ImageFormat.XSMALL:
				return formats.xsmall?.url;
			case ImageFormat.SMALL:
				return formats.small?.url;
			case ImageFormat.MEDIUM:
				return formats.medium?.url;
			case ImageFormat.LARGE:
				return formats.large?.url;
			case ImageFormat.XLARGE:
				return formats.xlarge?.url;
		}
	}

	return imageResource.data.attributes.url;
}

export function getImageSrcSet(image: ImageResource) {
	return Object.values(ImageFormat)
		.map((value) => {
			const url = getImageUrl(image, value);

			if (url && breakpoints[value]) {
				return `${url} ${breakpoints[value]}w`;
			}

			return null;
		})
		.filter(Boolean)
		.join(', ');
}
