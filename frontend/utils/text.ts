export function splitTextByHalf(text: string) {
	const half = Math.round(text.length / 2);
	const spaceIndexBack = text.slice(0, half).lastIndexOf(' ');
	const spaceIndexForward = text.slice(half).indexOf(' ') + half;

	const spaceIndex =
		Math.abs(spaceIndexBack - half) < Math.abs(spaceIndexForward - half)
			? spaceIndexBack
			: spaceIndexForward;
	return [text.slice(0, spaceIndex), '\n', text.slice(spaceIndex + 1)].join('');
}
