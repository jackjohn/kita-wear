import { defineStore } from 'pinia';
import { useLocalStorage } from '@vueuse/core';
import { CartProduct } from '@app/types/CartProduct';

export const useCartStore = defineStore('cart', {
	state: () => {
		return { products: useLocalStorage('cartProducts', [] as CartProduct[]) };
	},
	actions: {
		addProduct(productId: number, sizeId: number) {
			const productInCart = this.products.find(
				(cartProduct) => cartProduct.id === productId && cartProduct.sizeId === sizeId,
			);

			if (productInCart) {
				productInCart.quantity = productInCart.quantity + 1;
			} else {
				this.products.push({
					id: productId,
					quantity: 1,
					sizeId,
				});
			}
		},
		removeProduct(productId: number, productSizeId: number) {
			const productToRemoveIndex = this.products.findIndex(
				({ id, sizeId }) => id === productId && productSizeId === sizeId,
			);

			if (productToRemoveIndex !== -1) {
				this.products.splice(productToRemoveIndex, 1);
			}
		},
		removeAllProducts() {
			this.products = [];
		},
		changeProductQuantity(productId: number, productSizeId: number, quantity: number) {
			const productInCart = this.products.find(
				({ id, sizeId }) => id === productId && sizeId === productSizeId,
			);

			if (productInCart) {
				productInCart.quantity = quantity;
			}
		},
	},
	persist: true,
});
