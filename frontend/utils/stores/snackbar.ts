import { defineStore } from 'pinia';

export interface Notification {
	text: string;
}

export const useSnackbarStore = defineStore('snackbar', {
	state: () => {
		return { notification: null as Notification | null };
	},
	actions: {
		showNotification(notification: Notification) {
			this.notification = notification;
		},
		clearNotification() {
			this.notification = null;
		},
	},
});
