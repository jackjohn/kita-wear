import { describe, expect, test } from 'vitest';
import { mockProducts } from '#app/utils/tests/mocks';
import {
	getAllCartProductsPrice,
	getCartShippingPrice,
	getCartSubtotalWithPromoCodePrice,
	getProductFromCartSizeCount,
	mapProductsFromCart,
	validateCartForm,
} from '#app/utils/cart';
import { Product } from '#app/api/product';
import { ProductFromCart } from '#app/types/CartProduct';

describe('mapProductsFromCart', () => {
	test('should not return product which does not exist', () => {
		const mockCartProducts = [
			{
				id: mockProducts[0].id,
				quantity: 1,
				sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
			},
			{
				id: 9999,
				quantity: 2,
				sizeId: 99999,
			},
		];

		const result = mapProductsFromCart(mockProducts, mockCartProducts);
		expect(result).toMatchObject([
			{
				...mockProducts[0],
				cartInfo: mockCartProducts[0],
				disabled: false,
			},
		]);
	});

	test('should disable product which has zero quantity', () => {
		const mockCartProducts = [
			{
				id: mockProducts[0].id,
				quantity: 1,
				sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
			},
		];

		const mockProductsZeroQuantity: Product = {
			...mockProducts[0],
			attributes: {
				...mockProducts[0].attributes,
				ProductSizeToQuantity: [
					{
						...mockProducts[0].attributes.ProductSizeToQuantity![0],
						Quantity: '0',
					},
				],
			},
		};

		const result = mapProductsFromCart([mockProductsZeroQuantity], mockCartProducts);
		expect(result).toMatchObject([
			{
				...mockProductsZeroQuantity,
				cartInfo: {
					...mockCartProducts[0],
					quantity: 0,
				},
				disabled: true,
			},
		]);
	});
});

describe('getAllCartProductsPrice', () => {
	test('should return correct price for all cart products', () => {
		const mockCartProducts: ProductFromCart[] = [
			{
				...mockProducts[0],
				cartInfo: {
					id: mockProducts[0].id,
					quantity: 1,
					sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
				},
				disabled: false,
			},
			{
				...mockProducts[1],
				cartInfo: {
					id: mockProducts[1].id,
					quantity: 2,
					sizeId: mockProducts[1].attributes.ProductSizeToQuantity![0].product_size!.data.id,
				},
				disabled: true,
			},
			{
				...mockProducts[2],
				cartInfo: {
					id: mockProducts[2].id,
					quantity: 3,
					sizeId: mockProducts[3].attributes.ProductSizeToQuantity![0].product_size!.data.id,
				},
				disabled: false,
			},
		];

		const result = getAllCartProductsPrice(mockCartProducts);

		expect(result).equal(1485);
	});
});

describe('validateCartForm', () => {
	const validFields = {
		firstName: 'Jack',
		lastName: 'John',
		phoneNumber: '777888999',
		email: 'jackjohn@mail.com',
		inpostOffice: {} as any,
		privacyAgreement: true,
	};

	test('should return valid if all fields are correct', () => {
		const result = validateCartForm(validFields);
		expect(result).equal(true);
	});

	test('should return invalid if any field has length more than 100 characters', () => {
		const result = validateCartForm({
			...validFields,
			firstName: 'i'.repeat(101),
		});
		expect(result).equal(false);
	});

	test('should return invalid if any field is empty', () => {
		const result = validateCartForm({
			...validFields,
			firstName: '',
		});
		expect(result).equal(false);
	});

	test('should return invalid if any field has only empty spaces', () => {
		const result = validateCartForm({
			...validFields,
			firstName: '   ',
		});
		expect(result).equal(false);
	});

	test('should return invalid if email is invalid', () => {
		const result = validateCartForm({
			...validFields,
			email: 'test',
		});
		expect(result).equal(false);
	});
});

describe('getProductFromCartSizeCount', () => {
	test('should return count from cart info', () => {
		const result = getProductFromCartSizeCount({
			...mockProducts[0],
			cartInfo: {
				id: mockProducts[0].id,
				quantity: 1,
				sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
			},
			disabled: false,
		});
		expect(result).equal(1);
	});

	test('should return zero when product size does not exist anymore', () => {
		const result = getProductFromCartSizeCount({
			...mockProducts[0],
			attributes: {
				...mockProducts[0].attributes,
				ProductSizeToQuantity: [],
			},
			cartInfo: {
				id: mockProducts[0].id,
				quantity: 1,
				sizeId: mockProducts[0].attributes.ProductSizeToQuantity![0].product_size!.data.id,
			},
			disabled: false,
		});
		expect(result).equal(0);
	});
});

describe('getCartSubtotalWithPromoCodePrice', () => {
	test('should return correct price', () => {
		const result = getCartSubtotalWithPromoCodePrice(
			{
				code: 'WELCOME',
				percentage: 20,
			},
			280,
		);

		expect(result).equal(224);
	});
});

describe('getCartShippingPrice', () => {
	test('should return 15 when no free shipping for product and subtotal is too low', () => {
		const result = getCartShippingPrice({
			freeShippingForProduct: false,
			subtotalWithPromoCodePrice: 150,
			freeShippingFromPrice: 200,
			defaultShippingPrice: 15,
		});
		expect(result).equal(15);
	});

	test('should return zero when free shipping for product is available', () => {
		const result = getCartShippingPrice({
			freeShippingForProduct: true,
			subtotalWithPromoCodePrice: 100,
			freeShippingFromPrice: 200,
			defaultShippingPrice: 15,
		});
		expect(result).equal(0);
	});

	test('should return zero when subtotal with promo code is higher than minimal price for free shipping', () => {
		const result = getCartShippingPrice({
			freeShippingForProduct: false,
			subtotalWithPromoCodePrice: 300,
			freeShippingFromPrice: 200,
			defaultShippingPrice: 15,
		});
		expect(result).equal(0);
	});
});
