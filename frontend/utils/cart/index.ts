import { ApiResource } from '@app/api/types/ApiResource';
import { CartProduct, ProductFromCart } from '@app/types/CartProduct';
import { Product } from '@app/api/types/Product';
import { getProductSizeTitle } from '@app/utils/product';
import { isEmailValidator } from '@app/utils/form/validatiors';
import { DeliveryFormValues } from '@app/types/DeliveryFormValues';
import { OrderPayload } from '@app/types/OrderPayload';
import { computed } from '#imports';
import { PromoCodeValue } from '@app/types/PromoCodeValue';

export function mapProductsFromCart(
	products: ApiResource<Product>['data'][],
	cartProducts: CartProduct[],
): ProductFromCart[] {
	return cartProducts
		.map((cartProduct) => {
			const product = products.find((product) => product.id === cartProduct.id);
			const cartProductSize = product
				? product.attributes.ProductSizeToQuantity?.find(
						(item) => item.product_size?.data.id === cartProduct.sizeId,
				  )
				: null;

			if (product && cartProductSize) {
				let maxProductQuantity = parseInt(cartProductSize.Quantity);
				maxProductQuantity = Number.isNaN(maxProductQuantity) ? 0 : maxProductQuantity;
				return {
					...product,
					cartInfo: {
						...cartProduct,
						quantity:
							cartProduct.quantity <= maxProductQuantity
								? cartProduct.quantity
								: maxProductQuantity,
					},
					disabled: maxProductQuantity < cartProduct.quantity,
				};
			}

			return null;
		})
		.filter(Boolean) as any as ProductFromCart[];
}

export function getAllCartProductsPrice(cartProducts: ProductFromCart[]) {
	return cartProducts
		.filter(({ disabled }) => !disabled)
		.reduce((total, cartProduct) => {
			return total + cartProduct.attributes.Price * cartProduct.cartInfo.quantity;
		}, 0);
}

const FIELD_MAX_LENGTH = 100;

export function validateCartForm(formValues: DeliveryFormValues) {
	const invalidField = Object.keys(formValues).some((key) => {
		let fieldValue = formValues[key as keyof DeliveryFormValues];
		fieldValue = typeof fieldValue === 'string' ? fieldValue.trim() : fieldValue;

		if (typeof fieldValue === 'string' && fieldValue.length > FIELD_MAX_LENGTH) {
			return true;
		}

		if (key === 'promoCode') {
			return false;
		}

		if (key === 'email') {
			return !fieldValue || !isEmailValidator(fieldValue as string);
		}

		return !fieldValue;
	});

	return !invalidField;
}

export function generateOrderPayload({
	formValues,
	shippingPrice,
	totalPrice,
	cartProducts,
	promoCode,
}: {
	formValues: DeliveryFormValues;
	cartProducts: ProductFromCart[];
	promoCode: string | null;
	shippingPrice: number;
	totalPrice: number;
}): OrderPayload {
	return {
		firstName: formValues.firstName.trim(),
		lastName: formValues.lastName.trim(),
		email: formValues.email.trim(),
		phoneNumber: formValues.phoneNumber.trim(),
		inpostOfficePoint: formValues.inpostOffice!,
		promoCode: promoCode ? promoCode.trim() : null,
		products: cartProducts.map((cartProduct) => ({
			id: cartProduct.id,
			price: cartProduct.attributes.Price,
			quantity: cartProduct.cartInfo.quantity,
			sizeId: cartProduct.cartInfo.sizeId,
			name: cartProduct.attributes.Title,
			sizeTitle: getProductSizeTitle(cartProduct.attributes, cartProduct.cartInfo.sizeId)!,
		})),
		shippingPrice,
		totalPrice,
	};
}

export function getProductFromCartSizeCount(product: ProductFromCart) {
	return parseInt(
		product.attributes.ProductSizeToQuantity?.find(
			({ product_size }) => product_size?.data.id === product.cartInfo.sizeId,
		)?.Quantity || '0',
	);
}

export function getCartSubtotalWithPromoCodePrice(promoCode: PromoCodeValue, subTotal: number) {
	return subTotal - (subTotal * promoCode.percentage) / 100;
}

export function getCartShippingPrice({
	freeShippingForProduct,
	subtotalWithPromoCodePrice,
	freeShippingFromPrice,
	defaultShippingPrice,
}: {
	freeShippingForProduct: boolean;
	subtotalWithPromoCodePrice: number;
	freeShippingFromPrice: number;
	defaultShippingPrice: number;
}) {
	if (freeShippingForProduct || subtotalWithPromoCodePrice >= freeShippingFromPrice) {
		return 0;
	}

	return defaultShippingPrice;
}

export function getCartPrices({
	promoCode,
	freeShippingForProduct,
	allProductsPrice,
	freeShippingFromPrice,
	defaultShippingPrice,
}: {
	promoCode: PromoCodeValue | null;
	freeShippingForProduct: boolean;
	allProductsPrice: number;
	freeShippingFromPrice: number;
	defaultShippingPrice: number;
}) {
	const subtotalWithPromoCodePrice = promoCode
		? getCartSubtotalWithPromoCodePrice(promoCode, allProductsPrice)
		: allProductsPrice;
	const shippingPrice = getCartShippingPrice({
		freeShippingForProduct,
		defaultShippingPrice,
		freeShippingFromPrice,
		subtotalWithPromoCodePrice: subtotalWithPromoCodePrice,
	});
	const totalPrice = subtotalWithPromoCodePrice + shippingPrice;
	return {
		subtotalWithPromoCodePrice: subtotalWithPromoCodePrice,
		shippingPrice: shippingPrice,
		totalPrice: totalPrice,
	};
}
