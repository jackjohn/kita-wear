export enum ProductsSort {
	NEW = 'new',
	HIGH_PRICE = 'high_price',
	LOW_PRICE = 'low_price',
}
