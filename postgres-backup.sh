#!/bin/bash

# Get the container ID of the running Postgres container
CONTAINER_ID=$(docker ps -qf "name=postgres")

# Create a timestamped backup file
BACKUP_FILENAME="mydb_$(date +%Y%m%d_%H%M%S).sql"
docker exec $CONTAINER_ID pg_dump -U root -d strapi > $BACKUP_FILENAME

mkdir -p /var/opt/postgres-backups
mv ./$BACKUP_FILENAME /var/opt/postgres-backups/