'use strict';

/**
 * product-gender router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::product-gender.product-gender');
