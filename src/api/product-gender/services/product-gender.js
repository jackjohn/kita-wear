'use strict';

/**
 * product-gender service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::product-gender.product-gender');
