'use strict';

/**
 *  product-gender controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product-gender.product-gender');
