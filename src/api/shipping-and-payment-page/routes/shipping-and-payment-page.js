'use strict';

/**
 * shipping-and-payment-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::shipping-and-payment-page.shipping-and-payment-page');
