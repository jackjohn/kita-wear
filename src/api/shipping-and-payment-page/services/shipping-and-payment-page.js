'use strict';

/**
 * shipping-and-payment-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::shipping-and-payment-page.shipping-and-payment-page');
