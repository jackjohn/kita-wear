'use strict';

/**
 *  shipping-and-payment-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::shipping-and-payment-page.shipping-and-payment-page');
