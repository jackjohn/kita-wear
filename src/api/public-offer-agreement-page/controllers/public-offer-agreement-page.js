'use strict';

/**
 *  public-offer-agreement-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
	'api::public-offer-agreement-page.public-offer-agreement-page',
);
