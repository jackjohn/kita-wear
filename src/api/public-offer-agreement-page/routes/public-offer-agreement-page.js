'use strict';

/**
 * public-offer-agreement-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::public-offer-agreement-page.public-offer-agreement-page');
