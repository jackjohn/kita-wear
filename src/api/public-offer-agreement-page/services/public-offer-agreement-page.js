'use strict';

/**
 * public-offer-agreement-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::public-offer-agreement-page.public-offer-agreement-page');
