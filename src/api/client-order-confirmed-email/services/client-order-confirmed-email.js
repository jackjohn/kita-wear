'use strict';

/**
 * client-order-confirmed-email service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
	'api::client-order-confirmed-email.client-order-confirmed-email',
);
