'use strict';

/**
 *  client-order-confirmed-email controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
	'api::client-order-confirmed-email.client-order-confirmed-email',
);
