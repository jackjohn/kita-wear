'use strict';

/**
 * client-order-confirmed-email router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::client-order-confirmed-email.client-order-confirmed-email');
