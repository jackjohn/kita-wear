'use strict';

/**
 *  order-status-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::order-status-page.order-status-page');
