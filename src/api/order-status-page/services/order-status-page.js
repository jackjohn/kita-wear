'use strict';

/**
 * order-status-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::order-status-page.order-status-page');
