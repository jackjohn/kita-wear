'use strict';

/**
 * order-status-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::order-status-page.order-status-page');
