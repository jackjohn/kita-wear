'use strict';

/**
 * return-and-exchange-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::return-and-exchange-page.return-and-exchange-page');
