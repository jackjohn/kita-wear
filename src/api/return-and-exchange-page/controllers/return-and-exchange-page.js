'use strict';

/**
 *  return-and-exchange-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::return-and-exchange-page.return-and-exchange-page');
