'use strict';

/**
 * return-and-exchange-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::return-and-exchange-page.return-and-exchange-page');
