'use strict';

/**
 * order-checkout router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::order-checkout.order-checkout');
