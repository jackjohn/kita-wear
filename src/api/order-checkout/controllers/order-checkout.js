'use strict';

/**
 *  order-checkout controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::order-checkout.order-checkout');
