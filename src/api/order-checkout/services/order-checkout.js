'use strict';

/**
 * order-checkout service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::order-checkout.order-checkout');
