'use strict';

/**
 *  our-insta controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::our-insta.our-insta');
