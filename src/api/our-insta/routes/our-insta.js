'use strict';

/**
 * our-insta router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::our-insta.our-insta');
