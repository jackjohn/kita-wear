'use strict';

/**
 * our-insta service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::our-insta.our-insta');
