'use strict';

/**
 * products-sort-and-filter router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::products-sort-and-filter.products-sort-and-filter');
