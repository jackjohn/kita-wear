'use strict';

/**
 *  products-sort-and-filter controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::products-sort-and-filter.products-sort-and-filter');
