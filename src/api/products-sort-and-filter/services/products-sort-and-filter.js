'use strict';

/**
 * products-sort-and-filter service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::products-sort-and-filter.products-sort-and-filter');
