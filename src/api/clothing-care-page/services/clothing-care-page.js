'use strict';

/**
 * clothing-care-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::clothing-care-page.clothing-care-page');
