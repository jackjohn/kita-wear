'use strict';

/**
 *  clothing-care-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::clothing-care-page.clothing-care-page');
