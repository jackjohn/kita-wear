'use strict';

/**
 * clothing-care-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::clothing-care-page.clothing-care-page');
