import React, { useState } from 'react';
import { Button } from '@strapi/design-system/Button';

const PREVIEW_COOKIE_ID = 'preview';

export const TogglePreviewButton = () => {
	const [previewCookie, setPreviewCookie] = useState(getCookie(PREVIEW_COOKIE_ID));
	const togglePreview = () => {
		if (previewCookie) {
			eraseCookie(PREVIEW_COOKIE_ID);
		} else {
			setCookie(PREVIEW_COOKIE_ID, 'true', 365);
		}

		setPreviewCookie(getCookie(PREVIEW_COOKIE_ID));
	};

	return (
		<Button variant="secondary" onClick={togglePreview}>
			{previewCookie ? 'Disable preview' : 'Enable preview'}
		</Button>
	);
};

function setCookie(name, value, days) {
	const date = new Date();
	date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
	const expires = '; expires=' + date.toUTCString();
	document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

function getCookie(name) {
	const nameEQ = name + '=';
	const ca = document.cookie.split(';');

	for (let i = 0; i < ca.length; i++) {
		const c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1, c.length);
		}
		if (c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}
	return null;
}

function eraseCookie(name) {
	document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
