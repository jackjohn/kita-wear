import React from 'react';
import { Box, GridLayout } from '@strapi/design-system';
import { GenerateWebsiteButton } from '../GenerateWebsiteButton/index';
import { TogglePreviewButton } from '../TogglePreviewButton/index';

export const CustomActions = () => {
	return (
		<GridLayout>
			<Box />
			<Box>
				<GenerateWebsiteButton />
			</Box>
			<Box>
				<TogglePreviewButton />
			</Box>
		</GridLayout>
	);
};
