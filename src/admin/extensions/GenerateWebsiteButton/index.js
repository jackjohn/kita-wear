import React, { useState, useCallback } from 'react';
import { Button, Typography } from '@strapi/design-system';
import { request } from '@strapi/helper-plugin';

export const GenerateWebsiteButton = (props) => {
	const [deployError, setDeployError] = useState(null);

	const handleDeploy = useCallback(async () => {
		setDeployError(null);
		try {
			await request(`${process.env.STRAPI_ADMIN_APP_BASE_URL}/strapi-deploy`, { method: 'POST' });
		} catch (error) {
			setDeployError(error.response.payload.errorMessage);
		}
	}, []);

	return (
		<div style={{ whiteSpace: 'pre-wrap' }}>
			<Button type="button" variant="secondary" onClick={handleDeploy} error="checking">
				Deploy
			</Button>
			{deployError && (
				<Typography variant="pi" textColor="danger600" data-strapi-field-error>
					{deployError}
				</Typography>
			)}
		</div>
	);
};
