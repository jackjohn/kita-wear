import { CustomActions } from './extensions/CustomActions/index';

export default {
	bootstrap(app) {
		app.injectContentManagerComponent('editView', 'informations', {
			name: 'CustomActions',
			Component: CustomActions,
		});
	},
};
