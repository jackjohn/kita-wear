# Kita-wear

Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) and [strapi documentation](https://docs.strapi.io/developer-docs) to learn more.

## Prerequisites
* Docker

## Setup

1. Copy `.env.example` file, rename it to `.env` file and fill it with appropriate values
2. Run one of the following commands which will build all necessary images
```bash
    sh scripts/build-dev.sh # for development environment
    sh scripts/build-prod.sh # for production environment
```
3. Run one of the following command which will build and start all necessary containers
```bash
    sh scripts/run-dev.sh # for development environment
    sh scripts/run-prod.sh # for production environment
```

## Deploy
```bash
    sh scripts/deploy.sh
```