import { defineNuxtConfig } from 'nuxt/config';

let routes = [];

if (process.env.NODE_ENV === 'development') {
	require('dotenv').config();
}

if (process.env.NODE_ENV === 'production') {
	routes = require('./routes.json');
}

const previewConfig = {
	ssr: false,
	target: 'static',
	nitro: {
		output: {
			dir: '../.output-preview',
		},
		prerender: {
			routes,
		},
	},
};

const staticConfig = {
	ssr: true,
	target: 'static',
	nitro: {
		prerender: {
			crawlLinks: true,
			routes,
		},
	},
};

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
	alias: {
		'@app': '/app/frontend',
		'@app/*': '/app/frontend/*',
	},
	imports: {
		autoImport: false,
	},
	typescript: {
		strict: true,
	},
	runtimeConfig: {
		public: {
			APP_BASE_URL: process.env.APP_BASE_URL,
			STRAPI_API_TOKEN: process.env.STRAPI_API_TOKEN,
			INPOST_TOKEN: process.env.INPOST_TOKEN,
			siteUrl: process.env.APP_BASE_URL,
		},
	},
	...(process.env.NODE_ENV === 'production' && process.env.PREVIEW === 'true'
		? previewConfig
		: undefined),
	...(process.env.NODE_ENV === 'production' && process.env.PREVIEW !== 'true'
		? staticConfig
		: undefined),
	rootDir: './frontend',
	css: [
		'@/assets/styles/reset.scss',
		'@/assets/styles/global.scss',
		'@/assets/styles/fonts.scss',
		'@/../node_modules/vue3-carousel/dist/carousel.css',
	],
	modules: ['@pinia/nuxt', '@nuxt/image-edge', 'nuxt-simple-sitemap'],
	plugins: ['@/plugins/persistedstate.ts'],
	vue: {
		compilerOptions: {
			isCustomElement: (tag) => {
				if (tag === 'inpost-geowidget') {
					return true;
				}

				return false;
			},
		},
	},
	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					additionalData:
						'@use "@/assets/styles/_variables.scss" as *; @use "@/assets/styles/typography.scss" as *;',
				},
			},
		},
		...(process.env.NODE_ENV === 'development'
			? {
					server: {
						hmr: {
							protocol: 'ws',
							port: 9001,
						},
					},
			  }
			: undefined),
	},
});
