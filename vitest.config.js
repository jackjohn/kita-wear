import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';

export default defineConfig({
	plugins: [vue()],
	test: {
		globals: true,
		environment: 'jsdom',
		setupFiles: './frontend/utils/tests/setup.ts',
	},
	resolve: {
		alias: {
			'#build': path.resolve(__dirname, './frontend/.nuxt/imports.d.ts'),
			'#app': path.resolve(__dirname, './frontend'),
			'@app': path.resolve(__dirname, './frontend'),
			'#vue-router': path.resolve(__dirname, './frontend/.nuxt/vue-router.d.ts'),
			'#imports': path.resolve(__dirname, './frontend/base-imports.d.ts'),
			'#pinia': path.resolve(__dirname, './node_modules/pinia/dist/types.d.ts'),
			'#nuxt': path.resolve(__dirname, './node_modules/nuxt/dist/app/nuxt.d.ts'),
			'#vue-demi': path.resolve(__dirname, './node_modules/nuxt/dist/app/compat/vue-demi'),
		},
	},
});
