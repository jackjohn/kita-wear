module.exports = ({ env }) => ({
	host: env('HOST'),
	port: 1337,
	url: `${env('APP_BASE_URL')}/strapi`,
	app: {
		keys: env.array('APP_KEYS'),
	},
	dirs: {
		public: './frontend/public',
	},
});
