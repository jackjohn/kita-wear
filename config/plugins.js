const breakpoints = {
	xlarge: 1600,
	large: 1200,
	medium: 992,
	small: 700,
	xsmall: 500,
	xxsmall: 300,
};

module.exports = ({ env }) => ({
	'strapi-plugin-populate-deep': {
		config: {
			defaultDepth: 10,
		},
	},
	upload: {
		config: {
			breakpoints,
		},
	},
});
